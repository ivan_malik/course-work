CREATE TRIGGER city_BEFORE_DELETE
BEFORE DELETE ON city
FOR EACH ROW
  BEGIN
	delete from station where OLD.id_City = station.id_City;
  delete from mte where OLD.id_City = mte.id_City;
END;
