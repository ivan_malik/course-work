CREATE PROCEDURE findFreeSeats(IN idStartStation INT, IN jDate DATE, OUT res INT)
  BEGIN
	declare sum int(11);
	declare temp int(11);
	declare temp2 int(255);
    declare done integer default 0;
	declare cur cursor for
    select id_Route from route where (id_Trip = (select id_Trip from route where id_Route = idStartStation)) and id_Route >= idStartStation; 
	DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done=1;
	Open cur;
    set sum = 0;
    start_loop: loop
        fetch cur into temp;
            if done = 1 then 
                leave start_loop;
            else
                select sum(seat_count)  from ticket 
				where ticket.depature_station_route = temp and
				ticket.arrival_station_route in (select id_Route from route where id_Trip = (select id_Trip from route where id_Route = idStartStation))  and
				ticket.journey_date = jDate into temp2;
                if temp2 is not null then 
					set sum = sum + temp2;
				end if;
            end if;
    end loop;
    close cur;
    set @allSeats = (select seats from bus where id_Bus = (select id_Bus from trip where id_Trip = (select id_Trip from route where id_Route = idStartStation)));
	select (@allSeats - sum) into res;
END;
