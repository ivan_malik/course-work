<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css'>
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/myfooter.css'>
    <title>Агентам</title>
</head>
<body>
<nav class="navbar navbar-default  navbar-fixed" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="submit" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand"  href="${pageContext.request.contextPath}/"><img src="${pageContext.request.contextPath}/imgs/logo2.png" width="133" height="30"></a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="${pageContext.request.contextPath}/carriers"> Перевозчикам </a> </li>
                <li><a href="${pageContext.request.contextPath}/agents"> Агентам </a> </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="${pageContext.request.contextPath}/authorization"><i class="glyphicon glyphicon-user"></i>&nbspКабинет</a></li>
                <li><a href="${pageContext.request.contextPath}/admin"> <i class="glyphicon glyphicon-calendar"></i>&nbspРасписание</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="glyphicon glyphicon-info-sign"></i>&nbspСлужба поддержки <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#"><i class="glyphicon glyphicon-earphone"></i>&nbsp+38 (073) 65-013-01</a></li>
                        <li><a href="#">&nbsp&nbsp&nbsp&nbsp&nbsp+38 (097) 65-013-01</a></li>
                        <li><a href="#">&nbsp&nbsp&nbsp&nbsp&nbsp+38 (095) 65-013-01</a></li>
                        <li><a href="#">&nbsp&nbsp&nbsp&nbsp&nbsp+38 (063) 65-013-01</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="glyphicon glyphicon-envelope"></i>&nbsp&nbsp&nbsp&nbsp&nbspdertalius@gmail.com </a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div id="bus-about"><div class="container">
    <div class="col-sm-12">
        <a class="btn-back bus-icon-left-arrow-thin" href="${pageContext.request.contextPath}/">Вернуться на главную</a>
        <h1 class="h2 text-center">Агентам</h1>
        <h2 class="h4"><b>Приглашаем агентов к сотрудничеству по продаже автобусных билетов</b></h2><p><br>У вас появится возможность:<br><br><b>1.</b> Привлечь новых клиентов всех категорий<br><b>2.</b> Получить дополнительный доход<br><b>3.</b> Использовать удобный интерфейс для бронирования или API<br><b>4.</b> Получить доступ к маршрутной сети перевозчиков и автовокзалов<br><b>5.</b> Предоставить клиентам новую восстребованную услугу<br><br>
        </p><br><br><br>
        <div class="row col-sm-8">
            <h2 class="h4 col-sm-12"><b>Свяжитесь с нами:</b></h2>
        </div>
    </div>
</div>
    <div class="container">
        <div class="col-sm-12"><p></p><br><p class="text-center"><b>BusLogic – ваш билет на автобус!</b></p>
        </div>
    </div>
</div>
</body>
</html>
