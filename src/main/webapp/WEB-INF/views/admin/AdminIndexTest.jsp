<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css'>
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/myfooter.css'>
    <script type="text/javascript" src="${pageContext.request.contextPath}/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/sidebar.css'>
    <!-- Bootstrap core JavaScript-->
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/vendor/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/vendor/datatables/jquery.dataTables.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/js/sb-admin-datatables.min.js"></script>
    <!-- Custom fonts for this template-->
    <link href="${pageContext.request.contextPath}/bootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="${pageContext.request.contextPath}/bootstrap/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="${pageContext.request.contextPath}/bootstrap/css/sb-admin.css" rel="stylesheet">
    <title>Admin room</title>
</head>
<body>
<nav class="navbar navbar-default  navbar-fixed" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="submit" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand"  href="${pageContext.request.contextPath}/"><img src="${pageContext.request.contextPath}/imgs/logo2.png" width="133" height="30"></a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="${pageContext.request.contextPath}/carriers"> Перевозчикам </a> </li>
                <li><a href="${pageContext.request.contextPath}/agents"> Агентам </a> </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="${pageContext.request.contextPath}/authorization"><i class="glyphicon glyphicon-user"></i>&nbspКабинет</a></li>
                <li><a href="#"> <i class="glyphicon glyphicon-calendar"></i>&nbspРасписание</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="glyphicon glyphicon-info-sign"></i>&nbspСлужба поддержки <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#"><i class="glyphicon glyphicon-earphone"></i>&nbsp+38 (073) 65-013-01</a></li>
                        <li><a href="#">&nbsp&nbsp&nbsp&nbsp&nbsp+38 (097) 65-013-01</a></li>
                        <li><a href="#">&nbsp&nbsp&nbsp&nbsp&nbsp+38 (095) 65-013-01</a></li>
                        <li><a href="#">&nbsp&nbsp&nbsp&nbsp&nbsp+38 (063) 65-013-01</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="glyphicon glyphicon-envelope"></i>&nbsp&nbsp&nbsp&nbsp&nbspdertalius@gmail.com </a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="row " >
    <div class="col-sm-3">
        <div class="sidebar-nav">
            <div class="navbar navbar-default" role="navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <span class="visible-xs navbar-brand">Sidebar menu</span>
                </div>
                <div class="navbar-collapse collapse sidebar-navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><h3>Manage table</h3></li>
                        <li class="active"><a href="${pageContext.request.contextPath}/admin">City</a></li>
                        <li><a href="${pageContext.request.contextPath}/tobus">Bus</a></li>
                        <li><a href="${pageContext.request.contextPath}/tostation">Station</a></li>
                        <li><a href="${pageContext.request.contextPath}/todriver">Driver</a></li>
                        <li><a href="${pageContext.request.contextPath}/tomte">MTE</a></li>
                        <li><a  href="${pageContext.request.contextPath}/toreise">Reise</a></li>
                        <li><a href="${pageContext.request.contextPath}/totrip">Trip</a></li>
                        <li><a href="${pageContext.request.contextPath}/towi">WI</a></li>
                        <li class="divider"><a href="#">Users manage</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-9">
        <div class="card mb-3">
            <div class="card-header">
                 <h2>&nbsp;&nbsp;&nbsp;&nbsp;Cities List</h2>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <div id="dataTable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-4 col-md-4 col-lg-4">
                            </div>
                            <div class="col-sm-4 col-md-4 col-lg-4">
                            </div>
                            <div class="col-sm-12">
                                <a href="${pageContext.request.contextPath}/newCity" class="btn btn-primary btn-sm "><b>+</b> Add new city</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                        <thead>
                        <tr role="row">
                            <th class="sorting_asc" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 72px;">ID</th>
                            <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 111px;">Name</th>
                            <th class="text-center" style="width: 300px;">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach var="city" items="${cities}" >
                            <tr>
                                <td>${city.cityId}</td>
                                <td>${city.name}</td>
                                <td class="text-center">
                                    <a class='btn btn-info btn-xs' href="${pageContext.request.contextPath}/editCity?cityId=${city.cityId}"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                                    <a href="${pageContext.request.contextPath}/deleteCity?cityId=${city.cityId}" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Del</a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-5">
                                <!--<div class="dataTables_info" id="dataTable_info" role="status" aria-live="polite">Showing 1 to 10 of ${cities.size()} entries</div>--></div>
                            <div class="col-sm-12 col-md-7">
                                <div class="dataTables_paginate paging_simple_numbers" id="dataTable_paginate">
                                    <ul class="pagination">
                                       </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
