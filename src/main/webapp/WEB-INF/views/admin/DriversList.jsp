<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css'>
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/myfooter.css'>
    <script type="text/javascript" src="${pageContext.request.contextPath}/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/sidebar.css'>
    <!-- Bootstrap core JavaScript-->
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/vendor/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/vendor/datatables/jquery.dataTables.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/js/sb-admin-datatables.min.js"></script>
    <!-- Custom fonts for this template-->
    <link href="${pageContext.request.contextPath}/bootstrap/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Page level plugin CSS-->
    <link href="${pageContext.request.contextPath}/bootstrap/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
    <!-- Custom styles for this template-->
    <link href="${pageContext.request.contextPath}/bootstrap/css/sb-admin.css" rel="stylesheet">
    <title>Drivers management</title>
</head>
<body>
<div class="row " >
    <div class="col-sm-3">
        <div class="sidebar-nav">
            <div class="navbar navbar-default" role="navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <span class="visible-xs navbar-brand">Sidebar menu</span>
                </div>
                <div class="navbar-collapse collapse sidebar-navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><h3>Manage table</h3></li>
                        <li><a href="${pageContext.request.contextPath}/admin">City</a></li>
                        <li><a href="${pageContext.request.contextPath}/tobus">Bus</a></li>
                        <li><a href="${pageContext.request.contextPath}/tostation">Station</a></li>
                        <li class="active"><a href="${pageContext.request.contextPath}/todriver">Driver</a></li>
                        <li ><a href="${pageContext.request.contextPath}/tomte">MTE</a></li>
                        <li><a  href="${pageContext.request.contextPath}/toreise">Reise</a></li>
                        <li><a href="#">Route</a></li>
                        <li><a href="${pageContext.request.contextPath}/totrip">Trip</a></li>
                        <li><a href="${pageContext.request.contextPath}/towi">WI</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Users manage</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-9">
        <div class="card mb-3">
            <div class="card-header">
                <h2>&nbsp;&nbsp;&nbsp;&nbsp;Drivers List</h2>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <div id="dataTable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-4 col-md-4 col-lg-4">
                            </div>
                            <div class="col-sm-4 col-md-4 col-lg-4">
                            </div>
                            <div class="col-sm-12">
                                <a href="${pageContext.request.contextPath}/newDriver" class="btn btn-primary btn-sm "><b>+</b> Add new driver</a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                    <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-sort="ascending"  style="width: 72px;">ID</th>
                                        <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1"  style="width: 111px;">First name</th>
                                        <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 111px;">Last name</th>
                                        <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1"  style="width: 111px;">Phone number</th>
                                        <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1"  style="width: 111px;">MTE</th>
                                        <th class="text-center" style="width: 300px;">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="driver" items="${drivers}" >
                                        <tr>
                                            <td>${driver.driverId}</td>
                                            <td>${driver.firstName}</td>
                                            <td>${driver.lastName}</td>
                                            <td>${driver.phoneNumber}</td>
                                            <td>${driver.driverMte.title}</td>
                                            <td class="text-center">
                                                <a class='btn btn-info btn-xs' href="${pageContext.request.contextPath}/editDriver?driverId=${driver.driverId}"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                                                <a href="${pageContext.request.contextPath}/deleteDriver?driverId=${driver.driverId}" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Del</a>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 col-md-5">
                                <div class="col-sm-12 col-md-7">
                                    <div class="dataTables_paginate paging_simple_numbers" id="dataTable_paginate">
                                        <ul class="pagination">
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>