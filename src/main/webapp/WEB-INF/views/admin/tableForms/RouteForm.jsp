<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css'>
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/myfooter.css'>
    <script type="text/javascript" src="${pageContext.request.contextPath}/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
    <title>Route table</title>
</head>
<body>
<div align="center">
    <h1>New/Edit Route</h1>
    <table>
        <div class="form-group">
            <form:form action="/saveRoute" method="post" modelAttribute="route" >
                <form:hidden path="routeId"/>
                <tr>
                    <td>Station on route:</td>
                    <td>
                        <form:input path="stationsNumberOnRoute" cssClass="form-control"/></td>
                    </td>
                </tr>
                <tr>
                    <td>Distance from zero station:</td>
                    <td>
                        <form:input path="distanceFromeZeroStations" cssClass="form-control"/></td>
                    </td>
                </tr>
                <tr>
                    <td>Arrival time:</td>
                    <td>
                        <form:input path="arrivalTime" cssClass="form-control"/></td>
                    </td>
                </tr>
                <tr>
                    <td>Depature time:</td>
                    <td>
                        <form:input path="depatureTime" cssClass="form-control"/></td>
                    </td>
                </tr>
                <td>Station</td>
                <td>
                    <form:select path="stationId" cssClass="form-control">
                        <c:forEach var="station" items="${stations}" varStatus="status">
                            <form:option value="${station.stationId}" label="${station.stationName}" />
                        </c:forEach>
                    </form:select>
                </td>
                <td>
                    <form:hidden path="tripId"/>
                </td>
                <tr>
                    <td colspan="2" align="center">
                        <input type="submit" value="Save" class="btn btn-default">
                    </td>
                </tr>
            </form:form>
        </div>
    </table>
</div>
</body>
</html>

