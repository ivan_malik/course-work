<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css'>
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/myfooter.css'>
    <script type="text/javascript" src="${pageContext.request.contextPath}/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
    <title>Mte table</title>
</head>
<body>
<div align="center">
    <h1>New/Edit Mte</h1>
    <table>
        <div class="form-group">
            <form:form action="/saveMte" method="post" modelAttribute="mte" >
                <form:hidden path="mteId"/>
                <tr>
                    <td>Title:</td>
                    <td><form:input path="title" cssClass="form-control"/></td>
                </tr>
                <tr>
                    <td>Phone number:</td>
                    <td><form:input path="phoneNumber" cssClass="form-control"/></td>
                </tr>
                <tr>
                    <td>Address:</td>
                    <td><form:input path="address" cssClass="form-control"/></td>
                </tr>
                <td>City</td>
                <td>
                    <form:select path="cityId" cssClass="form-control">
                        <c:forEach var="city" items="${cities}" varStatus="status">
                            <form:option value="${city.cityId}" label="${city.name}" />
                        </c:forEach>
                    </form:select>
                </td>
                <td>
                </td>
                <tr>
                    <td colspan="2" align="center">
                        <input type="submit" value="Save" class="btn btn-default">
                    </td>
                </tr>
            </form:form>
        </div>
    </table>
</div>
</body>
</html>
