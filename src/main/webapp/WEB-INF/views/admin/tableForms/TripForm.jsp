<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css'>
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/myfooter.css'>
    <script type="text/javascript" src="${pageContext.request.contextPath}/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
    <title>Station table</title>
</head>
<body>
<div align="center">
    <h1>New/Edit Trip</h1>
    <table>
        <div class="form-group">
            <form:form action="/saveTrip" method="post" modelAttribute="trip" >
                <form:hidden path="tripId"/>
                <tr>
                    <td>Working interval:</td>
                    <td>
                        <form:select path="workingIntervalId" cssClass="form-control">
                            <c:forEach var="wi" items="${intervals}" varStatus="status">
                                <form:option value="${wi.wiId}" label="${wi.title}" />
                            </c:forEach>
                        </form:select>
                    </td>
                </tr>
                <tr>
                <td>Reise</td>
                <td>
                    <form:select path="reiseId" cssClass="form-control">
                        <c:forEach var="reise" items="${reiseList}" varStatus="status">
                            <form:option value="${reise.reiseId}" label="${reise.title}" />
                        </c:forEach>
                    </form:select>
                </td>
                </tr>
                <tr>
                <td>Bus</td>
                <td>
                    <form:select path="busId" cssClass="form-control">
                        <c:forEach var="bus" items="${buses}" varStatus="status">
                            <form:option value="${bus.busId}" label="${bus.model}(${bus.busMte.title})" />
                        </c:forEach>
                    </form:select>
                </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <input type="submit" value="Save" class="btn btn-default">
                    </td>
                </tr>
            </form:form>
        </div>
    </table>
</div>
</body>
</html>
