<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css'>
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/myfooter.css'>
    <script type="text/javascript" src="${pageContext.request.contextPath}/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
    <title>Bus table</title>
</head>
<body>
<div align="center">
    <h1>New/Edit bus</h1>
    <table>
        <div class="form-group">
            <form:form action="/saveBus" method="post" modelAttribute="bus" >
                <form:hidden path="busId"/>
                <tr>
                    <td>Class:</td>
                    <td><form:input path="clazz" cssClass="form-control"/></td>
                </tr>
                <tr>
                    <td>Model:</td>
                    <td><form:input path="model" cssClass="form-control"/></td>
                </tr>
                <tr>
                    <td>Price (UAH/km):</td>
                    <td><form:input path="priceUahKm" cssClass="form-control"/></td>
                </tr>
                <tr>
                    <td>Seats:</td>
                    <td><form:input path="seats" cssClass="form-control"/></td>
                </tr>
                <td>MTE</td>
                <td>
                    <form:select path="mteId" cssClass="form-control">
                        <c:forEach var="mte" items="${mteList}" varStatus="status">
                            <form:option value="${mte.mteId}" label="${mte.title}" />
                        </c:forEach>
                    </form:select>
                </td>
                <tr>
                    <td colspan="2" align="center">
                        <input type="submit" value="Save" class="btn btn-default">
                    </td>
                </tr>
            </form:form>
        </div>
    </table>
</div>
</body>
</html>