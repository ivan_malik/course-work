<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css'>
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/myfooter.css'>
    <script type="text/javascript" src="${pageContext.request.contextPath}/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
    <title>Station table</title>
</head>
<body>
<div align="center">
    <h1>New/Edit Working interval</h1>
    <table>
        <div class="form-group">
            <form:form action="/saveWI" method="post" modelAttribute="workingInterval" >
                <form:hidden path="wiId"/>
                <tr>
                    <td>Title:</td>
                    <td><form:input path="title" cssClass="form-control"/></td>
                </tr>
                <tr>
                    <td>Monday:</td>
                    <td><form:input path="monday" cssClass="form-control"/></td>
                </tr>
                <tr>
                    <td>Tuesday</td>
                    <td><form:input path="tuesday" cssClass="form-control"/></td>
                </tr>
                <tr>
                    <td>Wednesday</td>
                    <td><form:input path="wednesday" cssClass="form-control"/></td>
                </tr>
                <tr>
                    <td>Thursday</td>
                    <td><form:input path="thursday" cssClass="form-control"/></td>
                </tr>
                <tr>
                    <td>Friday</td>
                    <td><form:input path="friday" cssClass="form-control"/></td>
                </tr>
                <tr>
                    <td>Saturday</td>
                    <td><form:input path="saturday" cssClass="form-control"/></td>
                </tr>
                <tr>
                    <td>Sunday</td>
                    <td><form:input path="sunday" cssClass="form-control"/></td>
                </tr>
                <tr>
                    <td>Start date</td>
                    <td><form:input path="startDate" cssClass="form-control"/></td>
                </tr>
                <tr>
                    <td>End day</td>
                    <td><form:input path="endDate" cssClass="form-control"/></td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <input type="submit" value="Save" class="btn btn-default">
                    </td>
                </tr>
            </form:form>
        </div>
    </table>
</div>
</body>
</html>
