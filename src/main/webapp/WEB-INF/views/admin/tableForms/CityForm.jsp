<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css'>
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/myfooter.css'>
    <script type="text/javascript" src="${pageContext.request.contextPath}/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
    <title>City table</title>
</head>
<body>
<div align="center">
    <h1>New/Edit City</h1>
    <table>
        <div class="form-group">
        <form:form action="saveCity" method="post" modelAttribute="city" >
            <form:hidden path="cityId"/>
            <tr>
                <td>Title:</td>
                <td><form:input path="name" cssClass="form-control"/></td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <input type="submit" value="Save" class="btn btn-default">
                </td>
            </tr>
        </form:form>
        </div>
    </table>
</div>
</body>
</html>
