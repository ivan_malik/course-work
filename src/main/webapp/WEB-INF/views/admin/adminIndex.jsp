<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css'>
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/myfooter.css'>
    <script type="text/javascript" src="${pageContext.request.contextPath}/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/sidebar.css'>
    <title>Admin room</title>
</head>
<body>
    <div class="row " >
        <div class="col-sm-3">
            <div class="sidebar-nav">
                <div class="navbar navbar-default" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <span class="visible-xs navbar-brand">Sidebar menu</span>
                    </div>
                    <div class="navbar-collapse collapse sidebar-navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li><h3>Manage table</h3></li>
                            <li class="active"><a href="#">City</a></li>
                            <li><a href="#">Bus</a></li>
                            <li><a href="#">Station</a></li>
                            <li><a href="#">Driver</a></li>
                            <li><a href="#">MTE</a></li>
                            <li><a href="#">Reise</a></li>
                            <li><a href="#">Route</a></li>
                            <li><a href="#">Trip</a></li>
                            <li><a href="#">WI</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Users manage</a></li>
                        </ul>
                    </div><!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="col-sm-9">
            <h1>Cities List</h1>
            <div class="container">
                <div class="row col-md-6 col-md-offset-2 custyle">
                    <table class="table table-striped table-bordered ">
                        <thead>
                        <a href="${pageContext.request.contextPath}/newCity" class="btn btn-primary btn-xs pull-right"><b>+</b> Add new city</a>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <c:forEach var="city" items="${cities}" >
                        <tr>
                            <td>${city.cityId}</td>
                            <td>${city.name}</td>
                            <td class="text-center">
                                <a class='btn btn-info btn-xs' href="${pageContext.request.contextPath}/editCity?cityId=${city.cityId}"><span class="glyphicon glyphicon-edit"></span> Edit</a>
                                <a href="${pageContext.request.contextPath}/deleteCity?cityId=${city.cityId}" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span> Del</a>
                            </td>
                        </tr>
                    </c:forEach>
                    </table>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
