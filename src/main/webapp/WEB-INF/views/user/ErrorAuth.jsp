<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css'>
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/myfooter.css'>
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/auth.css'>
    <title>Authorization</title>
</head>
<body>
<div class="container">
    <div class="card card-container">
        <img id="profile-img" class="profile-img-card" src="${pageContext.request.contextPath}/imgs/avatar.png" />
        <p id="profile-name" class="profile-name-card"></p>
        <div class="alert alert-danger">
            <strong>Error with sign in!</strong> Wrong password or email. Try again.
        </div>
        <form:form action="${pageContext.request.contextPath}/check" method="post" class="form-signin" modelAttribute="credentials">
            <span id="reauth-email" class="reauth-email"></span>
            <form:input path="email"  type="email" id="inputEmail" class="form-control" placeholder="Email address" required="true"/>
            <form:input path="password" type="password" id="inputPassword" class="form-control" placeholder="Password" required="true"/>
            <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign in</button>
        </form:form>
        <a href="${pageContext.request.contextPath}/" class="forgot-password">Return back?</a>
        <br/>
        <a href="${pageContext.request.contextPath}/registration" class="forgot-password">New user? Register!</a>
    </div>
</div>
</body>
</html>
