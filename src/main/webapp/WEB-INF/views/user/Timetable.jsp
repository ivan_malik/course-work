<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css'>
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/myfooter.css'>
    <title>Просмотреть расписание</title>
</head>
<body>
<nav class="navbar navbar-default  navbar-fixed" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="submit" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand"  href="${pageContext.request.contextPath}/"><img src="${pageContext.request.contextPath}/imgs/logo2.png" width="133" height="30"></a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="${pageContext.request.contextPath}/carriers"> Перевозчикам </a> </li>
                <li><a href="${pageContext.request.contextPath}/agents"> Агентам </a> </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="${pageContext.request.contextPath}/authorization"><i class="glyphicon glyphicon-user"></i>&nbspКабинет</a></li>
                <li><a href="#"> <i class="glyphicon glyphicon-calendar"></i>&nbspРасписание</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="glyphicon glyphicon-info-sign"></i>&nbspСлужба поддержки <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#"><i class="glyphicon glyphicon-earphone"></i>&nbsp+38 (073) 65-013-01</a></li>
                        <li><a href="#">&nbsp&nbsp&nbsp&nbsp&nbsp+38 (097) 65-013-01</a></li>
                        <li><a href="#">&nbsp&nbsp&nbsp&nbsp&nbsp+38 (095) 65-013-01</a></li>
                        <li><a href="#">&nbsp&nbsp&nbsp&nbsp&nbsp+38 (063) 65-013-01</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="glyphicon glyphicon-envelope"></i>&nbsp&nbsp&nbsp&nbsp&nbspdertalius@gmail.com </a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container">
    <div>
        <div>
            <form action="${pageContext.request.contextPath}/timetable" method="get">
                <div class="form-group row">
                    <div class="col-xs-4">
                        <select class="form-control" name="from">
                            <c:forEach var="stn" items="${stations}" varStatus="status">
                                <option value="${stn.stationId}">${stn.stationName} (${stn.stationCity.name})</option>
                            </c:forEach>
                        </select>
                        <div><h5 style="color:#EAEDF1"> Выберите станцию</h5></div>
                    </div>
                    <div class="searchForm_group form-group">
                        <input type="submit" class="btn btn-default" value="Найти расписание">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

</body>
</html>
