<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css'>
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/myfooter.css'>
    <title>Детализация поездки</title>
</head>
<body>

<div class="panel-responsive panel-text">
    <div><a class="btn btn-default" href="${pageContext.request.contextPath}/">Вернуться к поиску</a></div>
    <div class="container">
        <div class="col-sm-12">
<div class="panel panel-success">
    <div class="panel-heading">Информация по рейсу</div>
    <div class="panel-body">
        <h1 class="h3 text-center">Автобус: ${model} </h1>
        <h1 class="h3 text-center">Перевозчик: ${mte} </h1>
        <h1 class="h3 text-center">Класс: ${clazz} </h1>
        <h1 class="h3 text-center">На рейсе осталось свободных мест: ${freeseats} </h1>
        <h1 class="h3 text-center">Маршрут следования: </h1>
        <c:forEach var="route" items="${routelist}" varStatus="status">
            <h3> Станция: ${route.stationOnRoute.stationName}, ${route.stationOnRoute.address}
            <br/> Время отправления/прибытия: ${route.arrivalTime} / ${route.depatureTime}</h3>
        </c:forEach>
    </div>
</div>
            <h2>Введите нужное количество билетов</h2>
            <div class="panel panel-default">
                <form action="#">
                    <div>
                        <input class="form-control" id="seats" name="seats" type="number" step="1" min="0" max=${freeseats} />
                    </div>
                    <div class="searchForm_group form-group">
                        <input type="submit" class="btn btn-default" value="Продолжить">
                    </div>
                </form>
            </div>
    </div>
    </div>
</div>

</body>
</html>
