<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css'>
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/myfooter.css'>
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/auth.css'>
    <title>Registration</title>
</head>
<body>
<div class="container">
    <div class="card card-container">
        <form:form action="saveUser" method="post" class="form-signin" role="form" modelAttribute="user">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <form:input path="firstName" type="text" name="first_name" id="first_name" class="form-control" placeholder="First Name" required="true"/>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <form:input path="lastName" type="text" name="last_name" id="last_name" class="form-control" placeholder="Last Name" required="true"/>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <form:input path="phoneNumber" type="text" name="phone" id="phone" class="form-control" placeholder="Phone number" required="true"/>
            </div>

            <div class="form-group">
                <form:input path="email" type="email" name="email" id="email" class="form-control" placeholder="Email Address" required="true"/>
            </div>

            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <form:input path="password" type="password" name="password" id="password" class="form-control" placeholder="Password" required="true"/>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" placeholder="Confirm Password" required>
                    </div>
                </div>
            </div>
            <input type="submit" value="Register" class="btn btn-lg btn-primary btn-block btn-signin">
        </form:form>
        <a href="${pageContext.request.contextPath}/" class="forgot-password">Return to start page?</a>
    </div>
</div>
</body>
</html>
