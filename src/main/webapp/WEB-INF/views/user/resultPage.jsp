<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib uri = "http://www.springframework.org/tags/form" prefix = "form"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css'>
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bootstrap/css/myfooter.css'>
    <title>Bus logic - продажа автобусных билетов по всей Украине</title>
</head>
<body>
<nav class="navbar navbar-default  navbar-fixed" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="submit" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand"  href="${pageContext.request.contextPath}/"><img src="${pageContext.request.contextPath}/imgs/logo2.png" width="133" height="30"></a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="#"> Перевозчикам </a> </li>
                <li><a href="#"> Агентам </a> </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="${pageContext.request.contextPath}/authorization"><i class="glyphicon glyphicon-user"></i>&nbspКабинет</a></li>
                <li><a href="${pageContext.request.contextPath}/admin"> <i class="glyphicon glyphicon-calendar"></i>&nbspРасписание</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="glyphicon glyphicon-info-sign"></i>&nbspСлужба поддержки <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="#"><i class="glyphicon glyphicon-earphone"></i>&nbsp+38 (073) 65-013-01</a></li>
                        <li><a href="#">&nbsp&nbsp&nbsp&nbsp&nbsp+38 (097) 65-013-01</a></li>
                        <li><a href="#">&nbsp&nbsp&nbsp&nbsp&nbsp+38 (095) 65-013-01</a></li>
                        <li><a href="#">&nbsp&nbsp&nbsp&nbsp&nbsp+38 (063) 65-013-01</a></li>
                        <li class="divider"></li>
                        <li><a href="#"><i class="glyphicon glyphicon-envelope"></i>&nbsp&nbsp&nbsp&nbsp&nbspdertalius@gmail.com </a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="well" style="background-image: url(${pageContext.request.contextPath}/imgs/bg.jpg); height: 70px;">
    <div >
        <div class="container">
            <div>
                <div>
                    <form action="${pageContext.request.contextPath}/result" method="get">
                        <div class="form-group row">
                            <div class="col-xs-4">
                                <select class="form-control" name="from">
                                    <c:forEach var="city" items="${cities}" varStatus="status">
                                        <option value="${city.cityId}">${city.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="col-xs-4">
                                <select class="form-control" name="to">
                                    <c:forEach var="city" items="${cities}" varStatus="status">
                                        <option value="${city.cityId}">${city.name}</option>
                                    </c:forEach>
                                </select>
                            </div>
                            <div class="col-xs-2">
                                <div>
                                    <input class="form-control" autocomplete="off" id="date" name="date" placeholder="date"  type="text" required/>
                                </div>
                            </div>
                            <div class="searchForm_group form-group">
                                <input type="submit" class="btn btn-default" value="Расписание и цены">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="panel panel-info center-block">
    <h3 align="center">Выберите подходящий рейс из найденных </h3>
</div>
<div class="row center-block" >
    <div class="col-sm-9">
        <div class="card mb-3">
            <div class="card-body">
                <div class="table-responsive">
                    <div id="dataTable_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4">
                        <div class="row">
                            <div class="col-sm-4 col-md-4 col-lg-4">
                            </div>
                            <div class="col-sm-4 col-md-4 col-lg-4">
                            </div>
                        </div>
                        <div class="row center-block" >
                            <div class="col-sm-12 center-block">
                                <table class="table table-bordered dataTable center-block" id="dataTable" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                                    <thead>
                                    <tr role="row">
                                        <th  tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 400px;">Отправление</th>
                                        <th  tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1"  style="width: 400px;">Прибытие</th>
                                        <th  tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 150px;">В пути</th>
                                        <th  tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1"  style="width: 150px;">Цена</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach var="searched" items="${searchingModels}" >
                                        <tr>
                                            <td>${searched.wayModel.routeWithStartStation.stationOnRoute.stationName} - ${searched.wayModel.routeWithStartStation.stationOnRoute.address} <br/>
                                                <strong>${searched.wayModel.routeWithStartStation.depatureTime}</strong>
                                            </td>
                                            <td>${searched.wayModel.routeWithEndStation.stationOnRoute.stationName} - ${searched.wayModel.routeWithStartStation.stationOnRoute.address} <br>
                                                <strong>${searched.wayModel.routeWithEndStation.arrivalTime}</strong>
                                            </td>
                                            <td> <strong>${searched.editJourneyTime} </strong></td>
                                            <td><a href="${pageContext.request.contextPath}/resulted?start=${searched.wayModel.routeWithStartStation.routeId}&jdate=${jDate}" class="btn btn-primary btn-sm ">Купить за ${searched.price} грн </a></td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="footer-bs">
    <div class="row">
        <div class="col-md-3 footer-brand animated fadeInLeft">
            <a class="navbar-brand"  href="/"><img src="${pageContext.request.contextPath}/imgs/logo2.png" width="133" height="30"></a>
            <p>Suspendisse hendrerit tellus laoreet luctus pharetra. Aliquam porttitor vitae orci nec ultricies. Curabitur vehicula, libero eget faucibus faucibus, purus erat eleifend enim, porta pellentesque ex mi ut sem.</p>
            <p>© 2017 BS3 UI Kit, All rights reserved</p>
        </div>
        <div class="col-md-4 footer-nav animated fadeInUp">
            <h4>Menu —</h4>
            <div class="col-md-6">
                <ul class="pages">
                    <li><a href="#">Travel</a></li>
                    <li><a href="#">Nature</a></li>
                    <li><a href="#">Explores</a></li>
                    <li><a href="#">Science</a></li>
                    <li><a href="#">Advice</a></li>
                </ul>
            </div>
            <div class="col-md-6">
                <ul class="list">
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Contacts</a></li>
                    <li><a href="#">Terms & Condition</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-2 footer-social animated fadeInDown">
            <h4>Follow Us</h4>
            <ul>
                <li><a href="#">Facebook</a></li>
                <li><a href="#">Twitter</a></li>
                <li><a href="#">Instagram</a></li>
                <li><a href="#">RSS</a></li>
            </ul>
        </div>
        <div class="col-md-3 footer-ns animated fadeInRight">
            <h4>Отзыв</h4>
            <p>Оставьте свой отзыв</p>
            <p>
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                        <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-envelope"></span></button>
                      </span>
            </div>
            </p>
        </div>
    </div>
</footer>

<script type="text/javascript" src="${pageContext.request.contextPath}/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<script>
    $(document).ready(function(){
        var date_input=$('input[name="date"]'); //our date input has the name "date"
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        date_input.datepicker({
            format: 'mm/dd/yyyy',
            container: container,
            startDate: '+0d',
            todayHighlight: true,
            autoclose: true
        })
    })
</script>
</body>
</html>
