package com.course.malik.dao;

import com.course.malik.model.City;
import com.course.malik.model.Driver;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class DriverDao {

    @Autowired
    private SessionFactory sessionFactory;

    public DriverDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public DriverDao() {
    }

    @Transactional
    public Driver findById(int id) {
        /*String hql = "from City where cityId= " + id;
        Query query = sessionFactory.getCurrentSession().createQuery(hql);

        @SuppressWarnings("unchecked")
        List<City> listUser = (List<City>) query.list();

        if (listUser != null && !listUser.isEmpty()) {
            return listUser.get(0);
        }

        return null;*/
        Driver driver = null;
        driver = (Driver) sessionFactory.getCurrentSession().get(Driver.class, id);
        Hibernate.initialize(driver);
        return driver;
    }

    @Transactional
    public void save(Driver driver) {
        sessionFactory.getCurrentSession().saveOrUpdate(driver);
    }

    @Transactional
    public void deleteById(int id) {
        Driver driver = new Driver();
        driver.setDriverId(id);
        sessionFactory.getCurrentSession().delete(driver);
    }

    @Transactional
    public List<Driver> findAll() {
        @SuppressWarnings("unchecked")
        TypedQuery<Driver> query = sessionFactory.getCurrentSession().createQuery("from Driver");
        return query.getResultList();
    }

    @Transactional
    public void delete(Driver driver) {
        sessionFactory.getCurrentSession().delete(driver);
    }
}
