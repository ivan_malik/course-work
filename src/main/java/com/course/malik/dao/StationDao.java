package com.course.malik.dao;

import com.course.malik.model.Station;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.sql.Date;
import java.util.List;

@Repository
public class StationDao {

    @Autowired
    private SessionFactory sessionFactory;

    public StationDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public StationDao() {
    }

    @Transactional
    public Station findById(int id) {
        Station station = null;
        station = (Station) sessionFactory.getCurrentSession().get(Station.class, id);
        Hibernate.initialize(station);
        return station;
    }

    @Transactional
    public void save(Station station) {
        sessionFactory.getCurrentSession().saveOrUpdate(station);
    }

    @Transactional
    public void deleteById(int id) {
        Station station = new Station();
        station.setStationId(id);
        sessionFactory.getCurrentSession().delete(station);
    }

    @Transactional
    public List<Station> findAll() {
        @SuppressWarnings("unchecked")
        TypedQuery<Station> query = sessionFactory.getCurrentSession().createQuery("from Station ");
        return query.getResultList();
    }

    @Transactional
    public void delete(Station station) {
        sessionFactory.getCurrentSession().delete(station);
    }

    @Transactional
    public List<Station> findStationByCity(int cityId) {
        final CriteriaBuilder cb =  sessionFactory.getCurrentSession().getCriteriaBuilder();
        final CriteriaQuery<Station> query = cb.createQuery(Station.class);
        final Root<Station> root = query.from(Station.class);
        query
                .where(cb.and(
                        cb.equal(root.get("stationCity").get("cityId"), cityId)
                ));
        return sessionFactory.getCurrentSession().createQuery(query).getResultList();
    }

}
