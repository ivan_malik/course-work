package com.course.malik.dao;

import com.course.malik.model.City;
import com.course.malik.model.Mte;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class MteDao {

    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    CityDao cityDao;

    public MteDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public MteDao() {
    }

    @Transactional
    public Mte findById(int id) {
        Mte mte = null;
        mte = (Mte) sessionFactory.getCurrentSession().load(Mte.class, id);
        Hibernate.initialize(mte);
        return mte;
    }

    @Transactional
    public void save(Mte mte) {
        sessionFactory.getCurrentSession().saveOrUpdate(mte);
    }

    @Transactional
    public void deleteById(int id) {
        Mte mte = new Mte();
        mte.setMteId(id);
        sessionFactory.getCurrentSession().delete(mte);
    }

    @Transactional
    public List<Mte> findAll() {
        @SuppressWarnings("unchecked")
        TypedQuery<Mte> query = sessionFactory.getCurrentSession().createQuery("from Mte");
        return query.getResultList();
    }

    @Transactional
    public void delete(Mte mte) {
        sessionFactory.getCurrentSession().delete(mte);
    }
}
