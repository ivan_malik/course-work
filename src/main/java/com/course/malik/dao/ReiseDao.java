package com.course.malik.dao;

import com.course.malik.model.Reise;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class ReiseDao {

    @Autowired
    private SessionFactory sessionFactory;

    public ReiseDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public ReiseDao() {
    }

    @Transactional
    public Reise findById(int id) {
        Reise reise = null;
        reise = (Reise) sessionFactory.getCurrentSession().get(Reise.class, id);
        Hibernate.initialize(reise);
        return reise;
    }

    @Transactional
    public void save(Reise reise) {
        sessionFactory.getCurrentSession().saveOrUpdate(reise);
    }

    @Transactional
    public void deleteById(int id) {
        Reise mte = new Reise();
        mte.setReiseId(id);
        sessionFactory.getCurrentSession().delete(mte);
    }

    @Transactional
    public List<Reise> findAll() {
        @SuppressWarnings("unchecked")
        TypedQuery<Reise> query = sessionFactory.getCurrentSession().createQuery("from Reise");
        return query.getResultList();
    }
    @Transactional
    public void delete(Reise reise) {
        sessionFactory.getCurrentSession().delete(reise);
    }
}
