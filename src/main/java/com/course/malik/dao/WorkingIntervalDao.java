package com.course.malik.dao;

import com.course.malik.model.WorkingInterval;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class WorkingIntervalDao {

    @Autowired
    private SessionFactory sessionFactory;

    public WorkingIntervalDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public WorkingIntervalDao() {
    }

    @Transactional
    public WorkingInterval findById(int id) {
        WorkingInterval interval = null;
        interval = (WorkingInterval) sessionFactory.getCurrentSession().get(WorkingInterval.class, id);
        Hibernate.initialize(interval);
        return interval;
    }

    @Transactional
    public void save(WorkingInterval interval) {
        sessionFactory.getCurrentSession().saveOrUpdate(interval);
    }

    @Transactional
    public void deleteById(int id) {
        WorkingInterval workingInterval = new WorkingInterval();
        workingInterval.setWiId(id);
        sessionFactory.getCurrentSession().delete(workingInterval);
    }

    @Transactional
    public List<WorkingInterval> findAll() {
        @SuppressWarnings("unchecked")
        TypedQuery<WorkingInterval> query = sessionFactory.getCurrentSession().createQuery("from WorkingInterval ");
        return query.getResultList();
    }

    @Transactional
    public void delete(WorkingInterval workingInterval) {
        sessionFactory.getCurrentSession().delete(workingInterval);
    }
}
