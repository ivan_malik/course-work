package com.course.malik.dao;

import com.course.malik.model.Bus;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class BusDao {

    @Autowired
    private SessionFactory sessionFactory;

    public BusDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public BusDao() {
    }

    @Transactional
    public Bus findById(int id) {
        /*String hql = "from City where cityId= " + id;
        Query query = sessionFactory.getCurrentSession().createQuery(hql);

        @SuppressWarnings("unchecked")
        List<City> listUser = (List<City>) query.list();

        if (listUser != null && !listUser.isEmpty()) {
            return listUser.get(0);
        }

        return null;*/
        Bus bus  = null;
        bus = (Bus) sessionFactory.getCurrentSession().get(Bus.class, id);
        Hibernate.initialize(bus);
        return bus;
    }

    @Transactional
    public void save(Bus bus) {
        sessionFactory.getCurrentSession().saveOrUpdate(bus);
    }

    @Transactional
    public void deleteById(int id) {
        Bus bus = new Bus();
        bus.setBusId(id);
        sessionFactory.getCurrentSession().delete(bus);
    }

    @Transactional
    public List<Bus> findAll() {
        @SuppressWarnings("unchecked")
        TypedQuery<Bus> query = sessionFactory.getCurrentSession().createQuery("from Bus");
        return query.getResultList();
    }

    @Transactional
    public void delete(Bus bus) {
        sessionFactory.getCurrentSession().delete(bus);
    }
}
