package com.course.malik.dao;

import com.course.malik.model.Route;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.sql.Date;
import java.sql.Time;
import java.util.List;

@Repository
public class RouteDao {

    @Autowired
    private SessionFactory sessionFactory;

    public RouteDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public RouteDao() {
    }

    @Transactional
    public Route findById(int id) {
        Route route = null;
        route = (Route) sessionFactory.getCurrentSession().get(Route.class, id);
        Hibernate.initialize(route);
        return route;
    }

    @Transactional
    public void save(Route route) {
        sessionFactory.getCurrentSession().saveOrUpdate(route);
    }

    @Transactional
    public void deleteById(int id) {
        Route route = new Route();
        route.setRouteId(id);
        sessionFactory.getCurrentSession().delete(route);
    }

    @Transactional
    public List<Route> findAll() {
        @SuppressWarnings("unchecked")
        TypedQuery<Route> query = sessionFactory.getCurrentSession().createQuery("from Route");
        return query.getResultList();
    }

    @Transactional
    public void delete(Route route) {
        sessionFactory.getCurrentSession().delete(route);
    }

    @Transactional
    public List<Route> findRouteByTrip(int tripId) {
        final CriteriaBuilder cb =  sessionFactory.getCurrentSession().getCriteriaBuilder();
        final CriteriaQuery<Route> query = cb.createQuery(Route.class);
        final Root<Route> root = query.from(Route.class);
        query
                .where(cb.and(
                        cb.equal(root.get("trip").get("tripId"), tripId)
                ));
        return sessionFactory.getCurrentSession().createQuery(query).getResultList();
    }

    @Transactional
    public List<Route> findTripsByFromToCityAndDate(Date date, int idCS, int idCE) {
        Query query = sessionFactory.getCurrentSession().getNamedQuery("callRoutePr")
                .setParameter("datet", date)
                .setParameter("idCityStart", idCS)
                .setParameter("idCityEnd", idCE);
        List<Route> result =  query.getResultList();
        return result;
    }

    @Transactional
    public Double findPrice(int idBus, int idStartRoute, int idRouteEnd) {
        StoredProcedureQuery query = sessionFactory.getCurrentSession()
                .createStoredProcedureQuery("findPrice")
                .registerStoredProcedureParameter(1, Integer.class, ParameterMode.IN)
                .registerStoredProcedureParameter(2, Integer.class, ParameterMode.IN)
                .registerStoredProcedureParameter(3, Integer.class, ParameterMode.IN)
                .registerStoredProcedureParameter(4, Double.class, ParameterMode.OUT)
                .setParameter(1, idBus)
                .setParameter(2, idStartRoute)
                .setParameter(3, idRouteEnd);

        query.execute();

        return (Double) query.getOutputParameterValue(4);
    }

    @Transactional
    public Time findJourneyTime(int idRouteStart, int idRouteEnd) {
        StoredProcedureQuery query = sessionFactory.getCurrentSession()
                .createStoredProcedureQuery("findJourneyTime")
                .registerStoredProcedureParameter(1, Integer.class, ParameterMode.IN)
                .registerStoredProcedureParameter(2, Integer.class, ParameterMode.IN)
                .registerStoredProcedureParameter(3, Time.class, ParameterMode.OUT)
                .setParameter(1, idRouteStart)
                .setParameter(2, idRouteEnd);

        query.execute();

        return (Time) query.getOutputParameterValue(3);
    }


    @Transactional
    public Integer findFreeSeats(int idRouteStartStation, Date jDate) {
        StoredProcedureQuery query = sessionFactory.getCurrentSession()
                .createStoredProcedureQuery("findFreeSeats")
                .registerStoredProcedureParameter(1, Integer.class, ParameterMode.IN)
                .registerStoredProcedureParameter(2, Date.class, ParameterMode.IN)
                .registerStoredProcedureParameter(3, Integer.class, ParameterMode.OUT)
                .setParameter(1, idRouteStartStation)
                .setParameter(2, jDate);

        query.execute();
        return (Integer) query.getOutputParameterValue(3);
    }
}
