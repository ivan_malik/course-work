package com.course.malik.dao;

import com.course.malik.model.UserRole;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class UserRoleDao {
    @Autowired
    private SessionFactory sessionFactory;

    public UserRoleDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public UserRoleDao() {
    }

    @Transactional
    public UserRole findById(int id) {
        UserRole userRole = null;
        userRole = (UserRole) sessionFactory.getCurrentSession().get(UserRole.class, id);
        Hibernate.initialize(userRole);
        return userRole;
    }

    @Transactional
    public void save(UserRole userRole) {
        sessionFactory.getCurrentSession().saveOrUpdate(userRole);
    }

    @Transactional
    public void deleteById(int id) {
        UserRole userRole = new UserRole();
        userRole.setRoleId(id);
        sessionFactory.getCurrentSession().delete(userRole);
    }

    @Transactional
    public List<UserRole> findAll() {
        @SuppressWarnings("unchecked")
        TypedQuery<UserRole> query = sessionFactory.getCurrentSession().createQuery("from UserRole ");
        return query.getResultList();
    }

    @Transactional
    public void delete(UserRole userRole) {
        sessionFactory.getCurrentSession().delete(userRole);
    }
}
