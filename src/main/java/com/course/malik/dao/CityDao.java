package com.course.malik.dao;

import com.course.malik.model.City;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.hibernate.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class CityDao {

    @Autowired
    private SessionFactory sessionFactory;

    public CityDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public CityDao() {
    }

    @Transactional
    public City findById(int id) {
        /*String hql = "from City where cityId= " + id;
        Query query = sessionFactory.getCurrentSession().createQuery(hql);

        @SuppressWarnings("unchecked")
        List<City> listUser = (List<City>) query.list();

        if (listUser != null && !listUser.isEmpty()) {
            return listUser.get(0);
        }

        return null;*/
        City city = null;
        city = (City) sessionFactory.getCurrentSession().get(City.class, id);
        Hibernate.initialize(city);
        return city;
    }

    @Transactional
    public void save(City city) {
        sessionFactory.getCurrentSession().saveOrUpdate(city);
    }

    @Transactional
    public void deleteById(int id) {
        City city = new City();
        city.setCityId(id);
        sessionFactory.getCurrentSession().delete(city);
    }

    @Transactional
    public List<City> findAll() {
        @SuppressWarnings("unchecked")
        TypedQuery<City> query = sessionFactory.getCurrentSession().createQuery("from City");
        return query.getResultList();
    }

    @Transactional
    public void delete(City city) {
        sessionFactory.getCurrentSession().delete(city);
    }
}
