package com.course.malik.dao;

import com.course.malik.model.Ticket;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class TicketDao {

    @Autowired
    private SessionFactory sessionFactory;

    public TicketDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public TicketDao() {
    }

    @Transactional
    public Ticket findById(int id) {
        Ticket ticket = null;
        ticket = (Ticket) sessionFactory.getCurrentSession().get(Ticket.class, id);
        Hibernate.initialize(ticket);
        return ticket;
    }

    @Transactional
    public void save(Ticket ticket) {
        sessionFactory.getCurrentSession().saveOrUpdate(ticket);
    }

    @Transactional
    public void deleteById(int id) {
        Ticket ticket = new Ticket();
        ticket.setTicketId(id);
        sessionFactory.getCurrentSession().delete(ticket);
    }

    @Transactional
    public List<Ticket> findAll() {
        @SuppressWarnings("unchecked")
        TypedQuery<Ticket> query = sessionFactory.getCurrentSession().createQuery("from Ticket ");
        return query.getResultList();
    }

    @Transactional
    public void delete(Ticket ticket) {
        sessionFactory.getCurrentSession().delete(ticket);
    }
}
