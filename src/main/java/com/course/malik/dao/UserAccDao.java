package com.course.malik.dao;

import com.course.malik.model.Route;
import com.course.malik.model.UserAccaunt;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class UserAccDao {

    @Autowired
    private SessionFactory sessionFactory;

    public UserAccDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public UserAccDao() {
    }

    @Transactional
    public UserAccaunt findById(int id) {
        UserAccaunt userAccaunt = null;
        userAccaunt = (UserAccaunt) sessionFactory.getCurrentSession().get(UserAccaunt.class, id);
        Hibernate.initialize(userAccaunt);
        return userAccaunt;
    }

    @Transactional
    public void save(UserAccaunt userAccaunt) {
        sessionFactory.getCurrentSession().saveOrUpdate(userAccaunt);
    }

    @Transactional
    public void deleteById(int id) {
        UserAccaunt userAccaunt = new UserAccaunt();
        userAccaunt.setUserAccauntId(id);
        sessionFactory.getCurrentSession().delete(userAccaunt);
    }

    @Transactional
    public List<UserAccaunt> findAll() {
        @SuppressWarnings("unchecked")
        TypedQuery<UserAccaunt> query = sessionFactory.getCurrentSession().createQuery("from UserAccaunt ");
        return query.getResultList();
    }

    @Transactional
    public void delete(UserAccaunt userAccaunt) {
        sessionFactory.getCurrentSession().delete(userAccaunt);
    }

    @Transactional
    public UserAccaunt findUserByEmailAndPassword (String email, String password) {
        final CriteriaBuilder cb =  sessionFactory.getCurrentSession().getCriteriaBuilder();
        final CriteriaQuery<UserAccaunt> query = cb.createQuery(UserAccaunt.class);
        final Root<UserAccaunt> root = query.from(UserAccaunt.class);
        query
                .where(cb.and(cb.equal(root.get("email"), email))).where(cb.and(cb.equal(root.get("password"), password)));

        return sessionFactory.getCurrentSession().createQuery(query).getSingleResult();
    }
}
