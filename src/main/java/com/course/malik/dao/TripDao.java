package com.course.malik.dao;

import com.course.malik.model.Trip;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class TripDao {

    @Autowired
    private SessionFactory sessionFactory;

    public TripDao(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public TripDao() {
    }

    @Transactional
    public Trip findById(int id) {
        Trip trip = null;
        trip = (Trip) sessionFactory.getCurrentSession().get(Trip.class, id);
        Hibernate.initialize(trip);
        return trip;
    }

    @Transactional
    public void save(Trip trip) {
        sessionFactory.getCurrentSession().saveOrUpdate(trip);
    }

    @Transactional
    public void deleteById(int id) {
        Trip trip = new Trip();
        trip.setTripId(id);
        sessionFactory.getCurrentSession().delete(trip);
    }

    @Transactional
    public List<Trip> findAll() {
        @SuppressWarnings("unchecked")
        TypedQuery<Trip> query = sessionFactory.getCurrentSession().createQuery("from Trip ");
        return query.getResultList();
    }

    @Transactional
    public void delete(Trip trip) {
        sessionFactory.getCurrentSession().delete(trip);
    }
}
