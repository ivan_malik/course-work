package com.course.malik.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.HashSet;
import java.util.Set;

@NamedNativeQueries({
        @NamedNativeQuery(
                name = "callRoutePr",
                query = "CALL findRoutesByCitiesAndDate(:datet, :idCityStart, :idCityEnd)",
                resultClass = Route.class
        )
})
@Entity
@Table(name = "route")
public class Route implements Serializable {

    @Id
    @Column(name = "id_Route")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int routeId;

    @Column(name = "stations_number_on_route")
    private int stationsNumberOnRoute;

    @Column(name = "distance_frome_zero_stations")
    private double distanceFromeZeroStations;

    @Column(name = "arrival_time")
    @Basic
    private Time arrivalTime;

    @Column(name = "depature_time")
    @Basic
    private Time depatureTime;

    @ManyToOne
    @JoinColumn(name = "id_Station", nullable = false)
    private Station stationOnRoute;

    @ManyToOne
    @JoinColumn(name = "id_Trip", nullable = false)
    private Trip trip;

    public Set<Ticket> getTicketArrivalStations() {
        return ticketArrivalStations;
    }

    public void setTicketArrivalStations(Set<Ticket> ticketArrivalStations) {
        this.ticketArrivalStations = ticketArrivalStations;
    }

    @OneToMany(mappedBy = "arrivalStationRoute", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Ticket> ticketArrivalStations = new HashSet<>();

    @OneToMany(mappedBy = "depatureStationRoute", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Ticket> ticketDepatureStations = new HashSet<>();

    public Route() {
        stationOnRoute = new Station();
    }

    public Route(int stationsNumberOnRoute, double distanceFromeZeroStations, Time arrivalTime, Time depatureTime, Station stationOnRoute, Trip trip) {
        this.stationsNumberOnRoute = stationsNumberOnRoute;
        this.distanceFromeZeroStations = distanceFromeZeroStations;
        this.arrivalTime = arrivalTime;
        this.depatureTime = depatureTime;
        this.stationOnRoute = stationOnRoute;
        this.trip = trip;
    }

    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public int getStationsNumberOnRoute() {
        return stationsNumberOnRoute;
    }

    public void setStationsNumberOnRoute(int stationsNumberOnRoute) {
        this.stationsNumberOnRoute = stationsNumberOnRoute;
    }

    public double getDistanceFromeZeroStations() {
        return distanceFromeZeroStations;
    }

    public void setDistanceFromeZeroStations(double distanceFromeZeroStations) {
        this.distanceFromeZeroStations = distanceFromeZeroStations;
    }

    public Set<Ticket> getTicketDepatureStations() {
        return ticketDepatureStations;
    }

    public void setTicketDepatureStations(Set<Ticket> ticketDepatureStations) {
        this.ticketDepatureStations = ticketDepatureStations;
    }

    public Time getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Time arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public Time getDepatureTime() {
        return depatureTime;
    }

    public void setDepatureTime(Time depatureTime) {
        this.depatureTime = depatureTime;
    }

    public Station getStationOnRoute() {
        return stationOnRoute;
    }

    public void setStationOnRoute(Station stationOnRoute) {
        this.stationOnRoute = stationOnRoute;
    }

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    public int getStationId() {
        return stationOnRoute.getStationId();
    }

    public void setStationId(int stationId) {
       stationOnRoute.setStationId(stationId);
    }

    public int getTripId() {
        return trip.getTripId();
    }

    public void setTripId(int tripId) {
        trip.setTripId(tripId);
    }


    @Override
    public String toString() {
        return "Route{" +
                "routeId=" + routeId +
                ", stationsNumberOnRoute=" + stationsNumberOnRoute +
                ", distanceFromeZeroStations=" + distanceFromeZeroStations +
                ", arrivalTime=" + arrivalTime +
                ", depatureTime=" + depatureTime +
                ", stationOnRoute=" + stationOnRoute.getStationName() +
                ", trip=" + trip.getTripId() +
                '}';
    }
}
