package com.course.malik.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "reise")
public class Reise {

    public Reise(String name) {
        this.title = name;
    }

    public Set<Trip> getTrips() {
        return trips;
    }

    public void setTrips(Set<Trip> trips) {
        this.trips = trips;
    }

    @Id
    @Column(name = "id_Reise")
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private int reiseId;

    @Column(name = "title")
    private String title;

    @OneToMany(mappedBy = "reise", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Trip> trips = new HashSet<>();

    public Reise() {
    }

    public Reise(int reiseId, String title) {

        this.reiseId = reiseId;
        this.title = title;
    }

    public int getReiseId() {
        return reiseId;
    }

    public void setReiseId(int reiseId) {
        this.reiseId = reiseId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Reise{" +
                "reiseId=" + reiseId +
                ", title='" + title + '\'' +
                '}';
    }
}
