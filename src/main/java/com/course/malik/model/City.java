package com.course.malik.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "city")
public class City {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_City")
    private int cityId;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "stationCity", cascade = {CascadeType.ALL}, orphanRemoval = true)
    private Set<Station> stations = new HashSet<>();

    @OneToMany(mappedBy = "mteCity", cascade = {CascadeType.ALL}, orphanRemoval = true)
    private Set<Mte> mtes = new HashSet<>();

    public City() {
    }

    public City(String name) {
        this.name = name;
    }

    public City(int id, String name) {
        this.cityId = id;
        this.name = name;
    }

    public Set<Station> getStations() {
        return stations;
    }

    public void setStations(Set<Station> stations) {
        this.stations = stations;
    }

    public Set<Mte> getMtes() {
        return mtes;
    }

    public void setMtes(Set<Mte> mtes) {
        this.mtes = mtes;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "City{" +
                "cityId=" + cityId +
                ", name='" + name + '\'' +
                '}';
    }
}
