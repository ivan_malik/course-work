package com.course.malik.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "station")
public class Station {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_Station")
    private int stationId;

    @Column(name = "name")
    private String stationName;

    @Column(name = "address")
    private String address;

    public Station() {
        stationCity = new City();
    }

    public Station(String stationName, String address, City stationCity) {
        this.stationName = stationName;
        this.address = address;
        this.stationCity = stationCity;
    }

    @ManyToOne
    @JoinColumn(name = "id_City", nullable = false)
    private City stationCity;

    @OneToMany(mappedBy = "stationOnRoute", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Route> routes = new HashSet<>();

    public Set<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(Set<Route> routes) {
        this.routes = routes;
    }

    public City getStationCity() {
        return stationCity;
    }

    public void setStationCity(City stationCity) {
        this.stationCity = stationCity;
    }

    public int getStationId() {
        return stationId;
    }

    public void setStationId(int stationId) {
        this.stationId = stationId;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getCityId() {
        return stationCity.getCityId();
    }

    public void setCityId(int idCity) {
        stationCity.setCityId(idCity);
    }

    @Override
    public String toString() {
        return "Station{" +
                "address='" + address + '\'' +
                ", stationId=" + stationId +
                ", stationName='" + stationName + '\'' +
                ", city=" + stationCity.getName() +
                '}';
    }
}
