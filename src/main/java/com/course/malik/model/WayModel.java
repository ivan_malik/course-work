package com.course.malik.model;

public class WayModel {
    private Route routeWithStartStation;
    private Route routeWithEndStation;

    public WayModel() {
    }

    public WayModel(Route routeWithStartStation, Route routeWithEndStation) {

        this.routeWithStartStation = routeWithStartStation;
        this.routeWithEndStation = routeWithEndStation;
    }

    public Route getRouteWithStartStation() {
        return routeWithStartStation;
    }

    public void setRouteWithStartStation(Route routeWithStartStation) {
        this.routeWithStartStation = routeWithStartStation;
    }

    public Route getRouteWithEndStation() {
        return routeWithEndStation;
    }

    public void setRouteWithEndStation(Route routeWithEndStation) {
        this.routeWithEndStation = routeWithEndStation;
    }
}
