package com.course.malik.model;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.sql.Date;

@Component
public class DataReise {
    private int cityFromId;
    private int cityToId;
    private String journeyDate;

    public DataReise() {
    }

    public DataReise(int cityFromId, int cityToId, String journeyDate) {
        this.cityFromId = cityFromId;
        this.cityToId = cityToId;
        this.journeyDate = journeyDate;
    }

    public int getCityFromId() {
        return cityFromId;
    }

    public Date dateAsSqlDate() {
        String[] date = journeyDate.split("/");
        String month = date[0];
        String day = date[1];
        String year = date[2];

        return new Date(Integer.parseInt(year)-1900, Integer.parseInt(month)-1, Integer.parseInt(day));
    }

    public void setCityFromId(int cityFromId) {
        this.cityFromId = cityFromId;
    }

    public int getCityToId() {
        return cityToId;
    }

    public void setCityToId(int cityToId) {
        this.cityToId = cityToId;
    }

    public String getJourneyDate() {
        return journeyDate;
    }

    public void setJourneyDate(String journeyDate) {
        this.journeyDate = journeyDate;
    }
}
