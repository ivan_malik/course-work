package com.course.malik.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "user_role")
public class UserRole {

    @Id
    @Column(name = "id_Role")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int roleId;

    @Column(name = "type")
    private String type;

    public Set<UserAccaunt> getUserAccaunts() {
        return userAccaunts;
    }

    public void setUserAccaunts(Set<UserAccaunt> userAccaunts) {
        this.userAccaunts = userAccaunts;
    }

    @OneToMany(mappedBy = "role", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<UserAccaunt> userAccaunts = new HashSet<>();

    public UserRole(String type) {
        this.type = type;
    }

    public UserRole() {

    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "UserRole{" +
                "roleId=" + roleId +
                ", type='" + type + '\'' +
                '}';
    }
}
