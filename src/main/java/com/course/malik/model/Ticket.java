package com.course.malik.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;

@Entity
@Table(name = "ticket")
public class Ticket {

    @Id
    @Column(name = "id_Ticket")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int ticketId;

    @Column(name = "seat_count")
    private int seatCount;

    @Column(name = "sales_date")
    @Basic
    private Date salesDate;

    @Column(name = "journey_date")
    @Basic
    private Date journeyDate;

    @Column(name = "price")
    private BigDecimal price;

    @ManyToOne
    @JoinColumn(name = "id_User", nullable = false)
    private UserAccaunt user;

    public Route getArrivalStationRoute() {
        return arrivalStationRoute;
    }

    public void setArrivalStationRoute(Route arrivalStationRoute) {
        this.arrivalStationRoute = arrivalStationRoute;
    }

    @ManyToOne
    @JoinColumn(name = "arrival_station_route", nullable = false)
    private Route arrivalStationRoute;

    public Route getDepatureStationRoute() {
        return depatureStationRoute;
    }

    public void setDepatureStationRoute(Route depatureStationRoute) {
        this.depatureStationRoute = depatureStationRoute;
    }

    @ManyToOne
    @JoinColumn(name = "depature_station_route", nullable = false)
    private Route depatureStationRoute;

    public Ticket(int seatCount, Date salesDate, Date journeyDate, BigDecimal price, UserAccaunt user, Route depatureStation, Route arrivalStation) {
        this.seatCount = seatCount;
        this.salesDate = salesDate;
        this.journeyDate = journeyDate;
        this.price = price;
        this.user = user;
        this.arrivalStationRoute = arrivalStation;
    }

    public Ticket() {
    }

    public int getTicketId() {
        return ticketId;
    }

    public void setTicketId(int ticketId) {
        this.ticketId = ticketId;
    }

    public int getSeatCount() {
        return seatCount;
    }

    public void setSeatCount(int seatCount) {
        this.seatCount = seatCount;
    }

    public Date getSalesDate() {
        return salesDate;
    }

    public void setSalesDate(Date salesDate) {
        this.salesDate = salesDate;
    }

    public Date getJourneyDate() {
        return journeyDate;
    }

    public void setJourneyDate(Date journeyDate) {
        this.journeyDate = journeyDate;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public UserAccaunt getUser() {
        return user;
    }

    public void setUser(UserAccaunt user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "TicketDao{" +
                "ticketId=" + ticketId +
                ", seatCount=" + seatCount +
                ", salesDate=" + salesDate +
                ", journeyDate=" + journeyDate +
                ", price=" + price +
                ", user=" + user.getLastName() +
                '}';
    }
}
