package com.course.malik.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "trip")
public class Trip {

    @Id
    @Column(name = "id_Trip")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int tripId;

    @ManyToOne
    @JoinColumn(name = "id_Reise")
    private Reise reise;

    @ManyToOne
    @JoinColumn(name = "id_WI")
    private WorkingInterval workingInterval;

    @ManyToOne
    @JoinColumn(name = "id_Bus")
    private Bus busTrip;

    @OneToMany(mappedBy = "trip", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Route> routes = new HashSet<>();

    public Set<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(Set<Route> routes) {
        this.routes = routes;
    }

    public Bus getBusTrip() {
        return busTrip;
    }

    public void setBusTrip(Bus busTrip) {
        this.busTrip = busTrip;
    }

    public Trip() {
        workingInterval = new WorkingInterval();
        reise = new Reise();
        busTrip = new Bus();
    }

    public Trip(Reise reise, WorkingInterval workingInterval) {
        this.reise = reise;
        this.workingInterval = workingInterval;
    }

    public int getTripId() {
        return tripId;
    }

    public void setTripId(int tripId) {
        this.tripId = tripId;
    }

    public Reise getReise() {
        return reise;
    }

    public void setReise(Reise reise) {
        this.reise = reise;
    }

    public WorkingInterval getWorkingInterval() {
        return workingInterval;
    }

    public void setWorkingInterval(WorkingInterval workingInterval) {
        this.workingInterval = workingInterval;
    }

    public int getWorkingIntervalId() {
        return workingInterval.getWiId();
    }

    public void setWorkingIntervalId(int workingIntervalId) {
        workingInterval.setWiId(workingIntervalId);
    }

    public int getReiseId() {
        return reise.getReiseId();
    }

    public void setReiseId(int reiseId) {
        reise.setReiseId(reiseId);
    }

    public int getBusId() {
        return busTrip.getBusId();
    }

    public void setBusId(int busId) {
        busTrip.setBusId(busId);
    }

    @Override
    public String toString() {
        return "Trip{" +
                "tripId=" + tripId +
                ", reise=" + reise +
                ", workingInterval=" + workingInterval +
                '}';
    }
}
