package com.course.malik.model;

import com.course.malik.dao.RouteDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Time;
import java.util.*;

@Component
public class SearchInfo {

    @Autowired
    private RouteDao routeDao;

    public List<Route> trimList(List<Route> list, List<Station> startStations, List<Station> endStations) {
        list = deleteRouteWithUniqueTripId(list);
        Comparator<Route> comparator = new RouteTripIdComparator();
        list.sort(comparator);

        List<Integer> idStartStationList = new ArrayList<>();
        for (Station startStation : startStations) idStartStationList.add(startStation.getStationId());
        List<Integer> idEndStationList = new ArrayList<>();
        for (Station endStation : endStations) idEndStationList.add(endStation.getStationId());

        List<Route> routesToDelete = new ArrayList<>();
        for (int i = 0; i < list.size(); i+=2) {
            if ( !( ((list.get(i).getStationsNumberOnRoute() < list.get(i+1).getStationsNumberOnRoute()) &&
                    ( idStartStationList.contains(list.get(i).getStationId()) ) ) ||
                    ((list.get(i).getStationsNumberOnRoute() > list.get(i+1).getStationsNumberOnRoute()) &&
                            ( idEndStationList.contains(list.get(i).getStationId()) ) ) ) ) {
                routesToDelete.add(list.get(i));
                routesToDelete.add(list.get(i+1));
            }
        }

        list.removeAll(routesToDelete);

        return list;
    }

    public List<SearchingModel> searchWays(List<Route> list ) {
        List<SearchingModel> searchingModels = new ArrayList<>();
        for (int i = 0; i < list.size(); i+=2) {
            int id = list.get(i).getTripId();
            int busId = list.get(i).getTrip().getBusId();
            WayModel wayModel = new WayModel(list.get(i), list.get(i+1));
            Time jTime = routeDao.findJourneyTime(list.get(i).getRouteId(), list.get(i+1).getRouteId());
            Double price = routeDao.findPrice(busId, list.get(i).getRouteId(), list.get(i+1).getRouteId());
            searchingModels.add(new SearchingModel(id, wayModel, jTime, price));
        }
        return searchingModels;
    }

    private List<Route> deleteRouteWithUniqueTripId(List<Route> list) {
        List<Route> routesToDelete = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            boolean f = true;
            for (int j = 0; j < list.size(); j++)
                if (list.get(i).getTripId() == list.get(j).getTripId() && (i != j)) {
                    f = false;
                    break;
                }
            if (f) {
                routesToDelete.add(list.get(i));
            }
        }
        list.removeAll(routesToDelete);
        return list;
    }

    private class RouteTripIdComparator implements Comparator<Route> {
        @Override
        public int compare(Route r1, Route r2) {
            if( r1.getTripId() > r2.getTripId() )
                return 1;
            else if( r1.getTripId() < r2.getTripId() )
                return -1;
            else
                return 0;
        }
    }



}
