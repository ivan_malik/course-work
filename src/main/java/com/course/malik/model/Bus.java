package com.course.malik.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "bus")
public class Bus {

    @Id
    @Column(name = "id_Bus")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int busId;

    public Mte getBusMte() {
        return busMte;
    }

    public void setBusMte(Mte busMte) {
        this.busMte = busMte;
    }

    @Column(name = "class")
    private String clazz;

    @Column(name = "model")
    private String model;

    @Column(name = "price_uah_km")
    private BigDecimal priceUahKm;

    @Column(name = "seats")
    private int seats;

    @ManyToOne
    @JoinColumn(name = "id_MTE", nullable = false)
    private Mte busMte;

    @OneToMany(mappedBy = "busTrip", cascade = {CascadeType.ALL}, orphanRemoval = true)
    private Set<Trip> trips = new HashSet<>();

    public Set<Trip> getTrips() {
        return trips;
    }

    public void setTrips(Set<Trip> trips) {
        this.trips = trips;
    }

    public Bus(String clazz, String model, BigDecimal priceUahKm, int seats) {
        this.clazz = clazz;
        this.model = model;
        this.priceUahKm = priceUahKm;
        this.seats = seats;
    }

    public Bus() {
        busMte =  new Mte();
    }

    public int getBusId() {
        return busId;
    }

    public void setBusId(int busId) {
        this.busId = busId;
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public BigDecimal getPriceUahKm() {
        return priceUahKm;
    }

    public void setPriceUahKm(BigDecimal priceUahKm) {
        this.priceUahKm = priceUahKm;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public int getMteId() {
        return busMte.getMteId();
    }

    public void setMteId(int mteId) {
        busMte.setMteId(mteId);
    }

    @Override
    public String toString() {
        return "Bus{" +
                "busId=" + busId +
                ", clazz='" + clazz + '\'' +
                ", model='" + model + '\'' +
                ", priceUahKm=" + priceUahKm +
                ", seats=" + seats +
                '}';
    }
}
