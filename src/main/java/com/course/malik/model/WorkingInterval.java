package com.course.malik.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "working_interval")
public class WorkingInterval {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_WI")
    private int wiId;

    @Column(name = "title")
    private String title;

    @Column(name = "monday")
    private Byte monday;

    @Column(name = "tuesday")
    private Byte tuesday;

    @Column(name = "wednesday")
    private Byte wednesday;

    @Column(name = "thursday")
    private Byte thursday;

    @Column(name = "friday")
    private Byte friday;

    @Column(name = "saturday")
    private Byte saturday;

    @Column(name = "sunday")
    private Byte sunday;

    @Column(name = "start_date")
    @Basic
    private Date startDate;

    @Column(name = "end_day")
    @Basic
    private Date endDate;

    @OneToMany(mappedBy = "workingInterval", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Trip> trips = new HashSet<>();

    public Set<Trip> getTrips() {
        return trips;
    }

    public void setTrips(Set<Trip> trips) {
        this.trips = trips;
    }

    public WorkingInterval() {
    }

    public WorkingInterval(int wiId, Byte monday, Byte tuesday, Byte wednesday, Byte thursday, Byte friday, Byte saturday, Byte sunday, Date startDate, Date endDate) {
        this.wiId = wiId;
        this.monday = monday;
        this.tuesday = tuesday;
        this.wednesday = wednesday;
        this.thursday = thursday;
        this.friday = friday;
        this.saturday = saturday;
        this.sunday = sunday;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getWiId() {
        return wiId;
    }

    public void setWiId(int wiId) {
        this.wiId = wiId;
    }

    public Byte getMonday() {
        return monday;
    }

    public void setMonday(Byte monday) {
        this.monday = monday;
    }

    public Byte getTuesday() {
        return tuesday;
    }

    public void setTuesday(Byte tuesday) {
        this.tuesday = tuesday;
    }

    public Byte getWednesday() {
        return wednesday;
    }

    public void setWednesday(Byte wednesday) {
        this.wednesday = wednesday;
    }

    public Byte getThursday() {
        return thursday;
    }

    public void setThursday(Byte thursday) {
        this.thursday = thursday;
    }

    public Byte getFriday() {
        return friday;
    }

    public void setFriday(Byte friday) {
        this.friday = friday;
    }

    public Byte getSaturday() {
        return saturday;
    }

    public void setSaturday(Byte saturday) {
        this.saturday = saturday;
    }

    public Byte getSunday() {
        return sunday;
    }

    public void setSunday(Byte sunday) {
        this.sunday = sunday;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "WorkingInterval{" +
                "idWi=" + wiId +
                ", monday=" + monday +
                ", tuesday=" + tuesday +
                ", wednesday=" + wednesday +
                ", thursday=" + thursday +
                ", friday=" + friday +
                ", saturday=" + saturday +
                ", sunday=" + sunday +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }
}
