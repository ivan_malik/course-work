package com.course.malik.model;

import org.springframework.stereotype.Component;

import java.sql.Date;
import java.sql.Time;

@Component
public class SearchingModel {

    private int idTrip;

    private WayModel wayModel;

    private Time journeyTime;

    private double price;

    private Date journeyDate;

    public Date getJourneyDate() {
        return journeyDate;
    }

    public void setJourneyDate(Date journeyDate) {
        this.journeyDate = journeyDate;
    }

    public SearchingModel(int idTrip, WayModel wayModel, Time journeyTime, double price) {
        this.idTrip = idTrip;
        this.wayModel = wayModel;
        this.journeyTime = journeyTime;
        this.price = price;
    }

    public SearchingModel() {
    }

    public String getEditJourneyTime() {
        String hstr, mstr;
        int hours = journeyTime.getHours();
        int minutes  = journeyTime.getMinutes();
        if (hours != 0)
            hstr = hours + " ч. ";
        else hstr = "";
        if(minutes != 0)
            mstr = minutes + " м.";
        else mstr = "";
        return hstr + mstr;
    }

    public int getIdTrip() {
        return idTrip;
    }

    public void setIdTrip(int idTrip) {
        this.idTrip = idTrip;
    }

    public WayModel getWayModel() {
        return wayModel;
    }

    public void setWayModel(WayModel wayModel) {
        this.wayModel = wayModel;
    }

    public Time getJourneyTime() {
        return journeyTime;
    }

    public void setJourneyTime(Time journeyTime) {
        this.journeyTime = journeyTime;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
