package com.course.malik.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "mte")
public class Mte {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_MTE")
    private int mteId;

    @Column(name = "title")
    private String title;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "address")
    private String address;

    @ManyToOne
    @JoinColumn(name = "id_City", nullable = false)
    private City mteCity;

    @OneToMany(mappedBy = "driverMte", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Driver> drivers = new HashSet<>();

    @OneToMany(mappedBy = "busMte", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Bus> buses = new HashSet<>();

    public Mte(String title, String phoneNumber, String address, City city) {
        this.title = title;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.mteCity = city;
    }

    public Mte() {
        mteCity = new City();
    }

    public int getMteId() {
        return mteId;
    }

    public City getMteCity() {
        return mteCity;
    }

    public void setMteCity(City mteCity) {
        this.mteCity = mteCity;
    }

    public void setMteId(int mteId) {
        this.mteId = mteId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Set<Driver> getDrivers() {
        return drivers;
    }

    public void setDrivers(Set<Driver> drivers) {
        this.drivers = drivers;
    }

    public Set<Bus> getBuses() {
        return buses;
    }

    public void setBuses(Set<Bus> buses) {
        this.buses = buses;
    }

    public int getCityId() {
        return mteCity.getCityId();
    }

    public void setCityId(int idCity) {
        mteCity.setCityId(idCity);
    }

    @Override
    public String toString() {
        return "Mte{" +
                "mteId=" + mteId +
                ", title='" + title + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", address='" + address + '\'' +
                ", city=" + mteCity.getName() +
                '}';
    }
}
