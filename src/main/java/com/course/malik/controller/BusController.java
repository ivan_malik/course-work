package com.course.malik.controller;

import com.course.malik.dao.BusDao;
import com.course.malik.dao.MteDao;
import com.course.malik.model.Bus;
import com.course.malik.model.Mte;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class BusController {

    @Autowired
    private MteDao mteDao;
    @Autowired
    private BusDao busDao;

    @RequestMapping(value = "tobus")
    public ModelAndView toBus() {
        List<Bus> buses = busDao.findAll();
        ModelAndView model = new ModelAndView("admin/BusList");
        model.addObject("buses", buses);
        return model;
    }

    @RequestMapping(value = "/newBus", method = RequestMethod.GET)
    public ModelAndView newBus() {
        ModelAndView model = new ModelAndView("admin/tableForms/BusForm");
        List<Mte> mteList = mteDao.findAll();
        model.addObject("mteList",mteList);
        model.addObject("bus", new Bus());
        return model;
    }

    @RequestMapping(value = "/editBus", method = RequestMethod.GET)
    public ModelAndView editBus(HttpServletRequest request) throws Exception {
        request.setCharacterEncoding("UTF-8");
        int busId = Integer.parseInt(request.getParameter("busId"));
        Bus bus = busDao.findById(busId);
        ModelAndView model = new ModelAndView("admin/tableForms/BusForm");
        List<Mte> mteList = mteDao.findAll();
        model.addObject("mteList",mteList);
        model.addObject("bus", bus);
        return model;
    }

    @RequestMapping(value = "/deleteBus", method = RequestMethod.GET)
    public ModelAndView deleteBus(HttpServletRequest request) throws Exception {
        request.setCharacterEncoding("UTF-8");
        int busId = Integer.parseInt(request.getParameter("busId"));
        Bus bus = busDao.findById(busId);
        busDao.delete(bus);
        return new ModelAndView("redirect:tobus");
    }

    @RequestMapping(value = "/saveBus", method = RequestMethod.POST)
    public ModelAndView saveStation(@ModelAttribute Bus bus) {
        busDao.save(bus);
        return new ModelAndView("redirect:tobus");
    }
}
