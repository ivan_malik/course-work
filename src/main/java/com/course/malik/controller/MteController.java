package com.course.malik.controller;


import com.course.malik.dao.CityDao;
import com.course.malik.dao.MteDao;
import com.course.malik.model.City;
import com.course.malik.model.Mte;
import com.course.malik.model.Station;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class MteController {

    @Autowired
    private MteDao mteDao;
    @Autowired
    private CityDao cityDao;

    @RequestMapping(value = "tomte")
    public ModelAndView toMte() {
        List<Mte> mteList = mteDao.findAll();
        ModelAndView model = new ModelAndView("admin/MteList");
        model.addObject("mteList", mteList);
        return model;
    }

    @RequestMapping(value = "/newMte", method = RequestMethod.GET)
    public ModelAndView newMte() {
        ModelAndView model = new ModelAndView("admin/tableForms/MteForm");
        List<City> cities = cityDao.findAll();
        model.addObject("cities",cities);
        model.addObject("mte", new Mte());
        return model;
    }

    @RequestMapping(value = "/editMte", method = RequestMethod.GET)
    public ModelAndView editMte(HttpServletRequest request) throws Exception {
        request.setCharacterEncoding("UTF-8");
        int mteId = Integer.parseInt(request.getParameter("mteId"));
        Mte mte = mteDao.findById(mteId);
        ModelAndView model = new ModelAndView("admin/tableForms/MteForm");
        List<City> cities = cityDao.findAll();
        model.addObject("cities",cities);
        model.addObject("mte", mte);
        return model;
    }

    @RequestMapping(value = "/deleteMte", method = RequestMethod.GET)
    public ModelAndView deleteMte(HttpServletRequest request) throws Exception {
        request.setCharacterEncoding("UTF-8");
        int mteId = Integer.parseInt(request.getParameter("mteId"));
        Mte mte = mteDao.findById(mteId);
        mteDao.delete(mte);
        return new ModelAndView("redirect:tomte");
    }

    @RequestMapping(value = "/saveMte", method = RequestMethod.POST)
    public ModelAndView saveMte(@ModelAttribute Mte mte) {
        mteDao.save(mte);
        return new ModelAndView("redirect:tomte");
    }
}
