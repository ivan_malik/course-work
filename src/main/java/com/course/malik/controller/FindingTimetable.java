package com.course.malik.controller;

import com.course.malik.dao.StationDao;
import com.course.malik.model.Station;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class FindingTimetable {

    @Autowired
    private StationDao stationDao;

    @RequestMapping(value = "chooseStation")
    public ModelAndView chooseStation() {
        List<Station> list = stationDao.findAll();
        ModelAndView modelAndView = new ModelAndView("user/Timetable");
        modelAndView.addObject("stations", list);
        return modelAndView;
    }

    @RequestMapping(value = "timetable")
    public ModelAndView timetable(HttpServletRequest request) {
        int idStation = Integer.valueOf(request.getParameter("from"));
        Station station = stationDao.findById(idStation);
        ModelAndView modelAndView = new ModelAndView("user/ResultTimetable");
        modelAndView.addObject("station", station);
        return modelAndView;
    }
}
