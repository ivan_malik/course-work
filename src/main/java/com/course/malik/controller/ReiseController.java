package com.course.malik.controller;

import com.course.malik.dao.ReiseDao;
import com.course.malik.model.Reise;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class ReiseController {

    @Autowired
    private ReiseDao reiseDao;

    @RequestMapping(value = "toreise")
    public ModelAndView toReise() {
        List<Reise> reiseList = reiseDao.findAll();
        ModelAndView model = new ModelAndView("admin/ReiseList");
        model.addObject("reiseList", reiseList);
        return model;
    }

    @RequestMapping(value = "/newReise", method = RequestMethod.GET)
    public ModelAndView newReise() {
        ModelAndView model = new ModelAndView("admin/tableForms/ReiseForm");
        model.addObject("reise", new Reise());
        return model;
    }

    @RequestMapping(value = "/editReise", method = RequestMethod.GET)
    public ModelAndView editReise(HttpServletRequest request) throws Exception {
        request.setCharacterEncoding("UTF-8");
        int reiseId = Integer.parseInt(request.getParameter("reiseId"));
        Reise reise = reiseDao.findById(reiseId);
        ModelAndView model = new ModelAndView("admin/tableForms/ReiseForm");
        model.addObject("reise", reise);
        return model;
    }

    @RequestMapping(value = "/deleteReise", method = RequestMethod.GET)
    public ModelAndView deleteReise(HttpServletRequest request) throws Exception {
        request.setCharacterEncoding("UTF-8");
        int reiseId = Integer.parseInt(request.getParameter("reiseId"));
        Reise reise = reiseDao.findById(reiseId);
        reiseDao.delete(reise);
        return new ModelAndView("redirect:toreise");
    }

    @RequestMapping(value = "/saveReise", method = RequestMethod.POST)
    public ModelAndView saveStation(@ModelAttribute Reise reise) {
        reiseDao.save(reise);
        return new ModelAndView("redirect:toreise");
    }
}
