package com.course.malik.controller;

import com.course.malik.dao.WorkingIntervalDao;
import com.course.malik.model.WorkingInterval;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class WIController {

    @Autowired
    private WorkingIntervalDao workingIntervalDao;

    @RequestMapping(value = "towi")
    public ModelAndView toWI() {
        List<WorkingInterval> intervals = workingIntervalDao.findAll();
        ModelAndView model = new ModelAndView("admin/WIList");
        model.addObject("wiList", intervals);
        return model;
    }

    @RequestMapping(value = "/newWI", method = RequestMethod.GET)
    public ModelAndView newWI() {
        ModelAndView model = new ModelAndView("admin/tableForms/WIForm");
        model.addObject("workingInterval", new WorkingInterval());
        return model;
    }

    @RequestMapping(value = "/editWI", method = RequestMethod.GET)
    public ModelAndView editWI(HttpServletRequest request) throws Exception {
        request.setCharacterEncoding("UTF-8");
        int wiId = Integer.parseInt(request.getParameter("wiId"));
        WorkingInterval workingInterval = workingIntervalDao.findById(wiId);
        ModelAndView model = new ModelAndView("admin/tableForms/WIForm");
        model.addObject("workingInterval", workingInterval);
        return model;
    }

    @RequestMapping(value = "/deleteWI", method = RequestMethod.GET)
    public ModelAndView deleteWI(HttpServletRequest request) throws Exception {
        request.setCharacterEncoding("UTF-8");
        int wiId = Integer.parseInt(request.getParameter("wiId"));
        WorkingInterval workingInterval = workingIntervalDao.findById(wiId);
        workingIntervalDao.delete(workingInterval);
        return new ModelAndView("redirect:towi");
    }

    @RequestMapping(value = "/saveWI", method = RequestMethod.POST)
    public ModelAndView saveWI(@ModelAttribute WorkingInterval workingInterval) {
        workingIntervalDao.save(workingInterval);
        return new ModelAndView("redirect:towi");
    }
}
