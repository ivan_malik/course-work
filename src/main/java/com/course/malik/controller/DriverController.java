package com.course.malik.controller;

import com.course.malik.dao.DriverDao;
import com.course.malik.dao.MteDao;
import com.course.malik.model.Driver;
import com.course.malik.model.Mte;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class DriverController {

    @Autowired
    private MteDao mteDao;
    @Autowired
    private DriverDao driverDao;

    @RequestMapping(value = "todriver")
    public ModelAndView toDriver() {
        List<Driver> drivers = driverDao.findAll();
        ModelAndView model = new ModelAndView("admin/DriversList");
        model.addObject("drivers", drivers);
        return model;
    }

    @RequestMapping(value = "/newDriver", method = RequestMethod.GET)
    public ModelAndView newDriver() {
        ModelAndView model = new ModelAndView("admin/tableForms/DriverForm");
        List<Mte> mteList = mteDao.findAll();
        model.addObject("mteList",mteList);
        model.addObject("driver", new Driver());
        return model;
    }

    @RequestMapping(value = "/editDriver", method = RequestMethod.GET)
    public ModelAndView editDriver(HttpServletRequest request) throws Exception {
        request.setCharacterEncoding("UTF-8");
        int driverId = Integer.parseInt(request.getParameter("driverId"));
        Driver driver = driverDao.findById(driverId);
        ModelAndView model = new ModelAndView("admin/tableForms/DriverForm");
        List<Mte> mteList = mteDao.findAll();
        model.addObject("mteList",mteList);
        model.addObject("driver", driver);
        return model;
    }

    @RequestMapping(value = "/deleteDriver", method = RequestMethod.GET)
    public ModelAndView deleteDriver(HttpServletRequest request) throws Exception {
        request.setCharacterEncoding("UTF-8");
        int driverId = Integer.parseInt(request.getParameter("driverId"));
        Driver driver = driverDao.findById(driverId);
        driverDao.delete(driver);
        return new ModelAndView("redirect:todriver");
    }

    @RequestMapping(value = "/saveDriver", method = RequestMethod.POST)
    public ModelAndView saveDriver(@ModelAttribute Driver driver) {
        driverDao.save(driver);
        return new ModelAndView("redirect:todriver");
    }
}
