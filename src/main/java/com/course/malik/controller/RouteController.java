package com.course.malik.controller;

import com.course.malik.dao.RouteDao;
import com.course.malik.dao.StationDao;
import com.course.malik.dao.TripDao;
import com.course.malik.model.Route;
import com.course.malik.model.Station;
import com.course.malik.model.Trip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class RouteController {

    @Autowired
    private StationDao stationDao;
    @Autowired
    private RouteDao routeDao;
    @Autowired
    private TripDao tripDao;

    @RequestMapping(value = "toroute")
    public ModelAndView toRoute(HttpServletRequest request) {
        int tripId = Integer.parseInt(request.getParameter("tripId"));
        List<Route> routes = routeDao.findRouteByTrip(tripId);
        ModelAndView model = new ModelAndView("admin/RouteList");
        model.addObject("routes", routes);
        model.addObject("tripId",tripId);
        return model;
    }

    @RequestMapping(value = "/newRoute", method = RequestMethod.GET)
    public ModelAndView newRoute(HttpServletRequest request) {
        ModelAndView model = new ModelAndView("admin/tableForms/RouteForm");
        List<Station> stations = stationDao.findAll();
        int tripId = Integer.parseInt(request.getParameter("tripId"));
        model.addObject("stations",stations);
        Route route = new Route();
        route.setTrip(tripDao.findById(tripId));
        model.addObject("route", route);
        return model;
    }

    @RequestMapping(value = "/editRoute", method = RequestMethod.GET)
    public ModelAndView editRoute(HttpServletRequest request) throws Exception {
        request.setCharacterEncoding("UTF-8");
        int routeId = Integer.parseInt(request.getParameter("routeId"));
        Route route = routeDao.findById(routeId);
        ModelAndView model = new ModelAndView("admin/tableForms/RouteForm");
        List<Station> stations = stationDao.findAll();
        model.addObject("stations",stations);
        model.addObject("route", route);
        return model;
    }

    @RequestMapping(value = "/deleteRoute", method = RequestMethod.GET)
    public ModelAndView deleteRoute(HttpServletRequest request) throws Exception {
        request.setCharacterEncoding("UTF-8");
        int routeId = Integer.parseInt(request.getParameter("routeId"));
        Route route = routeDao.findById(routeId);
        int tripId = route.getTripId();
        routeDao.delete(route);
        return new ModelAndView("redirect:toroute?tripId=" + tripId);
    }

    @RequestMapping(value = "/saveRoute", method = RequestMethod.POST)
    public ModelAndView saveRoute(@ModelAttribute Route route) {
        int tripId = route.getTripId();
        routeDao.save(route);
        return new ModelAndView("redirect:toroute?tripId=" + tripId);
    }
}
