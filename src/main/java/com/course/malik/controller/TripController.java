package com.course.malik.controller;

import com.course.malik.dao.*;
import com.course.malik.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class TripController {

    @Autowired
    private TripDao tripDao;
    @Autowired
    private ReiseDao reiseDao;
    @Autowired
    private WorkingIntervalDao workingIntervalDao;
    @Autowired
    private BusDao busDao;
    @Autowired
    private MteDao mteDao;
    @Autowired
    private CityDao cityDao;

    @RequestMapping(value = "totrip")
    public ModelAndView toTrip() {
        List<Trip> trips = tripDao.findAll();
        ModelAndView model = new ModelAndView("admin/TripList");
        model.addObject("trips", trips);
        return model;
    }

    @RequestMapping(value = "/newTrip", method = RequestMethod.GET)
    public ModelAndView newTrip() {
        ModelAndView model = new ModelAndView("admin/tableForms/TripForm");
        List<Reise> reiseList = reiseDao.findAll();
        List<WorkingInterval> intervals = workingIntervalDao.findAll();
        List<Bus> buses =  busDao.findAll();
        model.addObject("reiseList",reiseList);
        model.addObject("intervals", intervals);
        model.addObject("trip", new Trip());
        model.addObject("buses", buses);
        return model;
    }

    @RequestMapping(value = "/editTrip", method = RequestMethod.GET)
    public ModelAndView editTrip(HttpServletRequest request) throws Exception {
        request.setCharacterEncoding("UTF-8");
        int tripId = Integer.parseInt(request.getParameter("tripId"));
        Trip trip = tripDao.findById(tripId);
        ModelAndView model = new ModelAndView("admin/tableForms/TripForm");
        List<Reise> reiseList = reiseDao.findAll();
        List<Bus> buses =  busDao.findAll();
        List<WorkingInterval> intervals = workingIntervalDao.findAll();
        model.addObject("reiseList",reiseList);
        model.addObject("intervals", intervals);
        model.addObject("buses", buses);
        model.addObject("trip", trip);
        return model;
    }

    @RequestMapping(value = "/deleteTrip", method = RequestMethod.GET)
    public ModelAndView deleteTrip(HttpServletRequest request) throws Exception {
        request.setCharacterEncoding("UTF-8");
        int tripId = Integer.parseInt(request.getParameter("tripId"));
        Trip trip = tripDao.findById(tripId);
        tripDao.delete(trip);
        return new ModelAndView("redirect:totrip");
    }

    @RequestMapping(value = "/saveTrip", method = RequestMethod.POST)
    public ModelAndView saveStation(@ModelAttribute Trip trip) {
        Bus b = busDao.findById(trip.getBusId());
        Reise r = reiseDao.findById(trip.getReiseId());
        WorkingInterval w = workingIntervalDao.findById(trip.getWorkingIntervalId());
        trip.setWorkingInterval(w);
        trip.setReise(r);
        trip.setBusTrip(b);
        tripDao.save(trip);
        return new ModelAndView("redirect:totrip");
    }
}
