package com.course.malik.controller;

import com.course.malik.dao.CityDao;
import com.course.malik.dao.RouteDao;
import com.course.malik.dao.StationDao;
import com.course.malik.dao.TripDao;
import com.course.malik.model.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.util.List;

@Controller
public class StartController {

    @Autowired
    private CityDao cityDao;
    @Autowired
    private RouteDao routeDao;
    @Autowired
    private DataReise dataReise;
    @Autowired
    private StationDao stationDao;
    @Autowired
    private SearchInfo searchInfo;
    @Autowired
    private TripDao tripDao;

    private static final Logger logger = Logger.getLogger(StartController.class);

    @RequestMapping("/")
    public ModelAndView handleRequest() throws Exception {
        if(logger.isDebugEnabled()){
            logger.debug("getWelcome is executed!");
        }

        List<City> cities = cityDao.findAll();
        ModelAndView model = new ModelAndView("index");
        model.addObject("allCities", cities);
        return model;
    }

    @RequestMapping(value ="/result", method = RequestMethod.GET)
    public ModelAndView result(HttpServletRequest request){
        int cityFromId = Integer.parseInt(request.getParameter("from"));
        int cityToId = Integer.parseInt(request.getParameter("to"));
        String date = request.getParameter("date");
        dataReise.setJourneyDate(date);
        Date dateSql = dataReise.dateAsSqlDate();//TODO: change location of method dateAsSQLDate
        List<Station> startStations = stationDao.findStationByCity(cityFromId);
        List<Station> endStations = stationDao.findStationByCity(cityToId);
        List<Route> routeList = routeDao.findTripsByFromToCityAndDate(dateSql, cityFromId, cityToId);
        ModelAndView model = new ModelAndView("user/resultPage");
        routeList = searchInfo.trimList(routeList, startStations, endStations);
        List<SearchingModel> searchingModels = searchInfo.searchWays(routeList);
        model.addObject("searchingModels", searchingModels);
        model.addObject("routeList", routeList);
        List<City> cities = cityDao.findAll();
        model.addObject("cities", cities);
        model.addObject("jDate", dateSql);
        return model;
    }

    @RequestMapping(value = "/carriers")
    public String carriers() {
        return "Carriers";
    }

    @RequestMapping(value = "/agents")
    public String agents () {
        return "Agents";
    }

    @RequestMapping(value ="/resulted", method = RequestMethod.GET)
    public ModelAndView infAboutSearchedResult(HttpServletRequest request) {
        int id = Integer.parseInt(request.getParameter("start"));
        Date jdate = Date.valueOf(request.getParameter("jdate"));
        Integer seats = routeDao.findFreeSeats(id, jdate);
        Route r = routeDao.findById(id);
        int idTrip = r.getTripId();
        Trip trip = tripDao.findById(idTrip);
        Bus bus = trip.getBusTrip();
        String busModel = bus.getModel();
        String clszz = bus.getClazz();
        String mte = bus.getBusMte().getTitle();
        List<Route> routeList = routeDao.findRouteByTrip(idTrip);
        ModelAndView modelAndView = new ModelAndView("user/results");
        modelAndView.addObject("freeseats", seats);
        modelAndView.addObject("model", busModel);
        modelAndView.addObject("clazz", clszz);
        modelAndView.addObject("mte", mte);
        modelAndView.addObject("routelist", routeList);
        return modelAndView;
    }

    @RequestMapping(value = "/redirect", method = RequestMethod.GET)
    public String redirecthome() {
        return "redirect: homePage";
    }

    @RequestMapping(value = "/homePage", method = RequestMethod.GET)
    public String homePage() {
        return "user/ErrorAuth";
    }
}
