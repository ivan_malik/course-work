package com.course.malik.controller;

import com.course.malik.dao.CityDao;
import com.course.malik.dao.StationDao;
import com.course.malik.model.City;
import com.course.malik.model.Station;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class AdminController {

    @Autowired
    private CityDao cityDao;

    @RequestMapping(value = "admin")
    public ModelAndView admin() {
        List<City> cities = cityDao.findAll();
        ModelAndView model = new ModelAndView("admin/AdminIndexTest");
        model.addObject("cities", cities);
        return model;
    }

    @RequestMapping(value = "/newCity", method = RequestMethod.GET)
    public ModelAndView newCity() {
        ModelAndView model = new ModelAndView("admin/tableForms/CityForm");
        model.addObject("city", new City());
        return model;
    }

    @RequestMapping(value = "/editCity", method = RequestMethod.GET)
    public ModelAndView editCity(HttpServletRequest request) throws Exception {
        request.setCharacterEncoding("UTF-8");
        int userId = Integer.parseInt(request.getParameter("cityId"));
        City city = cityDao.findById(userId);
        ModelAndView model = new ModelAndView("admin/tableForms/CityForm");
        model.addObject("city", city);
        return model;
    }

    @RequestMapping(value = "/deleteCity", method = RequestMethod.GET)
    public ModelAndView deleteCity(HttpServletRequest request) throws Exception {
        request.setCharacterEncoding("UTF-8");
        int cityId = Integer.parseInt(request.getParameter("cityId"));
        cityDao.deleteById(cityId);
        return new ModelAndView("redirect:admin");
    }

    @RequestMapping(value = "/saveCity", method = RequestMethod.POST)
    public ModelAndView saveCity(@ModelAttribute City city) {
        cityDao.save(city);
        return new ModelAndView("redirect:admin");
    }
}
