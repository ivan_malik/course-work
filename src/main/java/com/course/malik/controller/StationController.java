package com.course.malik.controller;

import com.course.malik.dao.CityDao;
import com.course.malik.dao.StationDao;
import com.course.malik.model.City;
import com.course.malik.model.Station;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class StationController {
    @Autowired
    private CityDao cityDao;
    @Autowired
    private StationDao stationDao;

    @RequestMapping(value = "tostation")
    public ModelAndView toStation() {
        List<Station> stations = stationDao.findAll();
        ModelAndView model = new ModelAndView("admin/StationList");
        model.addObject("stations", stations);
        return model;
    }

    @RequestMapping(value = "/newStation", method = RequestMethod.GET)
    public ModelAndView newStation() {
        ModelAndView model = new ModelAndView("admin/tableForms/StationForm");
        List<City> cities = cityDao.findAll();
        model.addObject("cities",cities);
        model.addObject("station", new Station());
        return model;
    }

    @RequestMapping(value = "/editStation", method = RequestMethod.GET)
    public ModelAndView editStation(HttpServletRequest request) throws Exception {
        request.setCharacterEncoding("UTF-8");
        int stationId = Integer.parseInt(request.getParameter("stationId"));
        Station station = stationDao.findById(stationId);
        ModelAndView model = new ModelAndView("admin/tableForms/StationForm");
        List<City> cities = cityDao.findAll();
        model.addObject("cities",cities);
        model.addObject("station", station);
        return model;
    }

    @RequestMapping(value = "/deleteStation", method = RequestMethod.GET)
    public ModelAndView deleteStation(HttpServletRequest request) throws Exception {
        request.setCharacterEncoding("UTF-8");
        int stationId = Integer.parseInt(request.getParameter("stationId"));
        Station station = stationDao.findById(stationId);
        stationDao.delete(station);
        return new ModelAndView("redirect:tostation");
    }

    @RequestMapping(value = "/saveStation", method = RequestMethod.POST)
    public ModelAndView saveStation(@ModelAttribute Station station) {
        stationDao.save(station);
        return new ModelAndView("redirect:tostation");
    }
}
