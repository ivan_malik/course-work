package com.course.malik.controller;

import com.course.malik.dao.CityDao;
import com.course.malik.dao.UserAccDao;
import com.course.malik.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.NoResultException;
import java.util.List;

@Controller
public class AuthorizationController {

    @Autowired
    private UserAccDao userAccDao;
    @Autowired
    private CityDao cityDao;

    @RequestMapping(value = "/authorization")
    public ModelAndView goAuth() {
        ModelAndView modelAndView = new ModelAndView("user/Authorization");
        modelAndView.addObject("credentials", new Credentials());
        return modelAndView;
    }

    @RequestMapping(value = "/registration")
    public ModelAndView goRegistr() {
        ModelAndView modelAndView = new ModelAndView("user/Registration");
        modelAndView.addObject("user", new UserAccaunt());
        return modelAndView;
    }

    @RequestMapping(value = "saveUser")
    public ModelAndView saveUser(@ModelAttribute UserAccaunt user) {
        UserRole role = new UserRole();
        role.setRoleId(1);
        user.setRole(role);
        userAccDao.save(user);
        return new ModelAndView("redirect: /");
    }

    @RequestMapping(value = "/check", method = RequestMethod.POST)
    public ModelAndView check(@ModelAttribute Credentials credentials) {
        ModelAndView modelAndView;
        UserAccaunt user;
        List<City> cities = cityDao.findAll();
        try {
            user = userAccDao.findUserByEmailAndPassword(credentials.getEmail(), credentials.getPassword());
            if (user == null) throw new Exception();
        }
        catch (Exception ex) {
            ModelAndView model = new ModelAndView("user/ErrorAuth");
            model.addObject("credentials", new Credentials());
            return model;
        }
            if (user.getRole().getRoleId() == 2) {
                ModelAndView model = new ModelAndView("admin/AdminIndexTest");
                model.addObject("cities", cities);
                return model;
            }
            else {
                ModelAndView model = new ModelAndView("index");
                model.addObject("allCities", cities);
                return model;
            }
    }

}
